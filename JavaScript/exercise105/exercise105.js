let distanceBetweenNodes = [];

function store() {
    let node1 = document.getElementById("value1").value.trim();
    let node2 = document.getElementById("value2").value.trim();
    let nodeDistance = document.getElementById("value3").value.trim();
    distanceBetweenNodes.push([...node1, ...node2, nodeDistance]);
    console.log(distanceBetweenNodes);
}
document.getElementById("storedata").addEventListener("click", store);

function myFunction() {
    let shortest;
    let start = document.getElementById("start").value.trim();
    let end = document.getElementById("end").value.trim();
    console.log(start, end);

    function findPath(start, end) {
        distanceBetweenNodes.map((item) => {
            if (item[0] === start && item[1] === end) {
                if (shortest > item[2]) {
                    shortest = item[2];
                }
                console.log(`the path ${start + end}  is ${item[2]}`);
            } else {
                distanceBetweenNodes.reduce((acc, val) => {
                    let addNode = [...acc, ...val];
                    if (addNode[0] == start && addNode[4] == end) {
                        if (addNode[1] == addNode[3]) {
                            let addTwoPath = parseInt(addNode[2]) + parseInt(addNode[5]);
                            if (shortest > addTwoPath) {
                                shortest = addTwoPath;
                            }
                            console.log(`the path ${start + addNode[1] + end} is ${addTwoPath}`);
                        }
                    } else {
                        distanceBetweenNodes.reduce((acc, val) => {
                            let threeNode = [...acc, ...val];
                            if (threeNode[0] === start && threeNode[7] === end) {
                                if (
                                    threeNode[1] === threeNode[3] &&
                                    threeNode[4] === threeNode[6]
                                ) {
                                    let addThreePath =
                                        parseInt(threeNode[2]) +
                                        parseInt(threeNode[5]) +
                                        parseInt(threeNode[8]);
                                    if (shortest > addThreePath) {
                                        shortest = addThreePath;
                                    }
                                    console.log(`the path ${start + threeNode[1] + threeNode[4] + end } is ${addThreePath}`);
                                }
                            }
                            return addNode;
                        });
                    }
                    return item;
                }), {};
            }
        });
        document.getElementById("return").innerHTML = `the shortest distance is ${shortest}`;
    }
    findPath(start, end);
}