function calculate() {
    //     let a = document.getElementById('num1').value;
    //     let b = document.getElementById('num2').value;
    //     let gcd;
    //     while (a != b) {
    //         if (a > b) {
    //             a = a - b;
    //         } else {
    //             b = b - a;
    //         }
    //     }
    //     gcd = a;
    //     document.getElementById('result').innerHTML = gcd;




    let hcf;
    const number1 = document.getElementById('num1').value;
    const number2 = document.getElementById('num2').value;
    for (let i = 1; i <= number1 && i <= number2; i++) {
        if (number1 % i === 0 && number2 % i === 0) {
            hcf = i;
        }
    }
    document.getElementById('result').innerHTML = `HCF of ${number1} and ${number2} is ${hcf}.`;
}