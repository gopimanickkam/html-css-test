function percentageCalculate() {
    let numerator = document.getElementById('numerator').value;
    let denominator = document.getElementById('denominator').value;
    let result = (numerator / denominator) * 100;
    document.getElementById('result').innerHTML = result + "%";
}