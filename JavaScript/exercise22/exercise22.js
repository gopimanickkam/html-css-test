function varSimplified() {
    var a = 10;
    document.getElementById('var1').innerHTML = a; //10 
    if (true) {
        var a = 20;
        document.getElementById('var2').innerHTML = a; //20
    }

    document.getElementById('var3').innerHTML = a; //20 



}

function letSimplified() {
    let b = 10;
    document.getElementById('let1').innerHTML = b; //10
    if (true) {
        let b = 20;
        document.getElementById('let2').innerHTML = b; //20
    }
    document.getElementById('let3').innerHTML = b; //10

    // let b = 20;
    // document.getElementById('let4').innerHTML = b;  //variable already declared

}

function constSimplified() {
    const c = 10;
    document.getElementById('const1').innerHTML = c;
    if (true) {
        const c = 20;
        document.getElementById('const2').innerHTML = c;
    }
    document.getElementById('const3').innerHTML = c;
    // const c=30
    //  c = 30;
    // document.getElementById('const4').innerHTML = c;
}