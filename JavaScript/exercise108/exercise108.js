function rolls(arr) {
    let base = [...arr];
    let sum = 0;
    for (let i = 0; i < base.length; i++) {
        if (base[i] == 1) {
            arr[i + 1] = 0;
        } else if (base[i] == 6) {
            arr[i + 1] = arr[i + 1] * 2;
        }
        sum = sum + arr[i];
    }
    return sum;
}
console.log(rolls([6, 6, 6, 6, 6]));