Array.prototype.mergeSort = function() {
    if (this.length <= 1) {
        return this;
    }

    let half = parseInt(this.length / 2);
    let left = this.slice(0, half).mergeSort();
    let right = this.slice(half, this.length).mergeSort();
    console.log(left, right);
    let merge = function(left, right) {
        let arry = [];
        while (left.length > 0 && right.length > 0) {
            arry.push((left[0] <= right[0]) ? left.shift() : right.shift());
        }
        return arry.concat(left).concat(right);
    };

    return merge(left, right);
};
let a = [34, 7, 23, 32, 5, 62];
console.log(a.mergeSort());