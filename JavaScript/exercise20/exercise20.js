function addFunction() {
    const num1 = document.getElementById('num1').value;
    const num2 = document.getElementById('num2').value;
    const sum = parseInt(num1) + parseInt(num2);
    document.getElementById('addNumber').innerHTML = (`The sum of ${num1} and ${num2} is ${sum}`);
}