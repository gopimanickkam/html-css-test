function fibo() {
    const num1 = document.getElementById('num1').value;
    let n1 = 0,
        n2 = 1,
        nextTerm;
    for (let i = 1; i <= num1; i++) {
        if (n1 <= num1)
            document.write(n1 + " ");
        nextTerm = n1 + n2;
        n1 = n2;
        n2 = nextTerm;
    }
}

function recur(num) {

    if (num == 1) {
        return [0, 1];
    } else {

        total = recur(num - 1);

        total.push(total[total.length - 1] + total[total.length - 2]);
        return total;
    }

}

document.write("Fibonacci Series of f(12) in JavaScript: " + recur(12) + "<br>");