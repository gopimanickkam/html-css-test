function myfunction() {
    let v1 = "string";
    let a = isNaN(v1);
    let v2 = ["red", "green", "blue", "yellow"];
    let b = Array.isArray(v2);
    let v3 = { id: 1 };
    let c = typeof v3;
    console.log(c);

    switch (a) {
        case true:
            document.getElementById("return1").innerHTML = "this is a string";
            break;

        default:
            document.getElementById("return1").innerHTML = "this is not a string";
    }

    switch (b) {
        case true:
            document.getElementById("return2").innerHTML = "this is a array";
            break;

        default:
            document.getElementById("return2").innerHTML = "this is not a array";
    }
    switch (c) {
        case "object":
            document.getElementById("return3").innerHTML = "this is a object";
            break;

        default:
            document.getElementById("return3").innerHTML = "this is not a object";
    }
}