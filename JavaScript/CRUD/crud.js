let arr = [];
const stateObject = {
    "India": ["Delhi",
        "Kerala",
        "Goa"
    ],
    "Australia": [
        "South Australia",
        "Victoria",
    ],
    "Canada": [
        "Alberta",
        "Columbia"
    ]
}
let formDataDetail = [];
let profileImageUrl = '';
let genderSelect;

const genderInput = `Male <input type="radio" id="male" name="gender" value="Male"  onkeypress="errorMessageRemove(event)" required> ` + `Female <input type="radio" id="female" name="gender" onkeypress="errorMessageRemove(event)" value="Female"> `;
const profileContainer = ` <div class="photoContainer">
<div class="photoUploadContainer">
<div class="photo">
<img src="" alt="" id="profileImage"></div>
<div class="photoUploadButton" id = 'photoUploadButtonId'>
</div>
<span class="error" id="errorImage"></span>
</div>

</div>
<div class="organizationContainer">
<div class="organizationName" id='organizationNameId'>Organization Name : <span class="starStyle">*</span><br>
</div><span class="error"  id="errorOraganizationName"></span>
<div class="firstName" id="firstNameId"></div>
</div>
<div class="nameContainer">
<div class="lastName" id="lastNameId"></div>
<div class="gender" id="genderId"></div></div>`;

const organizationData = ['VirdhiTech', 'Google', 'Facebook', 'Whatsapp']
const containerId = document.getElementById('containerId');
const profileContainerDiv = document.createElement('div');
profileContainerDiv.className = "profileContainer"
profileContainerDiv.innerHTML = profileContainer;
containerId.appendChild(profileContainerDiv);
photoUploadButtonId.innerHTML = `<input type="file" class ="uploadButton" id="uploadImageId" accept="image/png, image/jpeg" onchange="previewFile()">`


let profileimage = document.getElementById('profileImage');
let uploadButton = document.getElementById('uploadImageId');

function previewFile() {
    const preview = document.querySelector('img');
    const file = document.querySelector('input[type=file]').files[0];
    const reader = new FileReader();

    reader.addEventListener("load", function() {
        // convert image file to base64 string
        profileimage.style.display = 'block';
        preview.src = reader.result;
        // console.log(preview.src)
        profileImageUrl = preview.src;
    }, false);

    if (file) {

        reader.readAsDataURL(file);
    }
}



const organizationNameId = document.getElementById('organizationNameId');
let organizationNameIdSelect = document.createElement("SELECT");
organizationNameIdSelect.setAttribute("id", "myselect");
organizationNameId.appendChild(organizationNameIdSelect);

let organizationOption = document.createElement("option");
organizationOption.setAttribute("value", "name");
organizationOption.innerHTML = "Select organization";
document.getElementById("myselect").appendChild(organizationOption);
for (let i = 0; i < organizationData.length; i++) {
    let organizationOption = document.createElement("option");
    organizationOption.setAttribute("value", organizationData[i]);

    organizationOption.innerHTML = organizationData[i];
    document.getElementById("myselect").appendChild(organizationOption);
}
firstNameId.innerHTML = `First Name :  <span class="starStyle">*</span><br> ` + `<input type="text" class="firstNameinput" id="firstNameinputId" maxlength="20" onkeypress="errorMessageRemove(event)" onkeydown="return alphaOnly(event);"> <br> <span class="error" id="errorfirstName"></span>`;
lastNameId.innerHTML = `Last Name :  <span class="starStyle">*</span><br> ` + `<input type="text" class="lastNameinput" id="lastNameinputId" maxlength="20" onkeypress="errorMessageRemove(event)" onkeydown="return alphaOnly(event);" required><br> <span class="error" id="errorlastName"></span>`;
genderId.innerHTML = `Gender :  <span class="starStyle">*</span> <br> ` + genderInput + `<br><span class="error" id="errorgender"></span>`;
// const genderId = document.getElementById('genderId')

function errorMessageRemove(event) {
    document.getElementById(event.target.id).nextElementSibling.nextElementSibling.innerHTML = "";
}

function alphaOnly(event) {
    let key = event.keyCode;
    return ((key >= 65 && key <= 90) || key == 8 || key == 9);
};

function numericOnly(event) {
    let key = event.keyCode;
    return (key >= 48 && key <= 57 || key == 8);

}
const ContactDetailContainerDiv = document.createElement('div');
ContactDetailContainerDiv.className = "ContactDetailContainer";
ContactDetailContainerDiv.innerHTML =
    `<div class="dobContainer" id="dobContainerId">Date of Birth :  <span class="starStyle">*</span><br>
<input type="date" class="dobClass" id="dobId"required  onkeypress="errorMessageRemove(event)" onkeypress="errorMessageRemove(event)"> <br><span class="error" id="errordob" min="2000-01-02"></span>
</div>
<div class="mobileContainer" id="mobileContainerId" onkeypress="errorMessageRemove(event)">Mobile Number :  <span class="starStyle">*</span><br>
<input type="text" class="mobileNumber" id=mobileNumberId name="mobileNumber" maxlength="10" onkeypress="return numericOnly(event);" required ><br><span class="error" id="errormobile"></span>
</div>
<div class="mailIdContainer" id="mailIdContainerId" onkeypress="errorMessageRemove(event)" >Email ID:  <span class="starStyle">*</span><br>
<input type="email" class="mailIdClass" id="mailId" onkeydown="textAreaValidate('mailId');"><br><span class="error" id="errormail"></span><br>
</div>`
containerId.appendChild(ContactDetailContainerDiv);

const locationContainer = document.createElement('div');
locationContainer.className = "locationContainer"
locationContainer.innerHTML = `<div><div class="countryContainer " id="countryContainerId">
Country:   <span class="starStyle">*</span> <br> 
<select name="stateName" id="countryName" size="1">
<option value="Select Country" selected="selected">Select Country</option>
</select> 
</div>
<span class="error" id="errorcountry"><br>  
</div>

<div><div class="stateContainer" id="stateContainerId" >
  State:  <span class="starStyle">*</span> <br>
 <select name="countryName" id="stateName" size="1">
<option value="Please select Country first" selected="selected">Please select Country first</option>
</select>
</div>
<span class="error" id="errorstate"><br>
</div>
<div class="cityContainer" id="cityIdContainerId">City : <span class="starStyle">*</span><br>
    <input type="text" class="cityClass" id="cityId"  onkeypress="errorMessageRemove(event)" onkeydown="return alphaOnly(event);"> <br><span class="error" id="errorcity"></span>
</div>`;
containerId.appendChild(locationContainer);
//date validation

let date = new Date();
let year = date.getFullYear();
let month = date.getMonth() + 1;
let day = date.getDate();
year = 2015;


if (month < 10) {
    month = `0` + month;
}
if (day < 12) {
    day = `0` + day;
}
let maxDate = `${year}-${month}-${day}`;
document.getElementById("dobId").setAttribute("max", maxDate);

//country and state dropdown
window.onload = function() {
        let countryName = document.getElementById("countryName"),
            stateName = document.getElementById("stateName");

        for (let country in stateObject) {
            countryName.options[countryName.options.length] = new Option(country, country);
            // console.log(country, country)
        }
        countryName.onchange = function() {
            document.getElementById('errorcountry').innerHTML = "";
            stateName.length = 1; // remove all options bar first

            if (this.selectedIndex < 1) return; // done 
            let state = stateObject[this.value];
            for (let i = 0; i < state.length; i++) {


                stateName.options[stateName.options.length] = new Option(state[i], state[i]);
                // console.log(stateName.options.length)
            }
        }
    }
    //remove error msg for organization field
let organizationerror = document.getElementById('myselect');
if (organizationerror.value != 'Select Organization') {
    organizationerror.onchange = function() {
        document.getElementById('errorOraganizationName').innerHTML = "";
    }
}
//remove error msg for country field
let stateerror = document.getElementById('stateName');



stateerror.onchange = function() {
    console.log(stateerror.value != 'Please select Country first')
    if (stateerror.value != 'Please select Country first') {
        console.log('gopal')
        document.getElementById('errorstate').innerHTML = "";
    }
}
const addressContainer = document.createElement('div');
addressContainer.className = "addressContainer"
addressContainer.innerHTML = ` <div class="communicationAddress">
    Communication Address :  <span class="starStyle">*</span><br>
    <textarea name="" id="communicationAddr" style="resize:none" onkeypress="errorMessageRemove(event)" onkeydown="textAreaValidate('communicationAddr');"></textarea><br><span class="error" id="errorcommunication"></span><br></div>
    <div class="permanentAddress">
    Permanent Address :  <span class="starStyle">*</span><br>
    <textarea name="" id="permanentAddr" style="resize:none" onkeypress="errorMessageRemove(event)" onkeydown="textAreaValidate('permanentAddr');"></textarea><br><span class="error" id="errorpermanent"></span><br></div>`;
containerId.appendChild(addressContainer);

//textArea validation
function textAreaValidate(id) {
    var input = document.getElementById(id);
    input.addEventListener('keydown', function(e) {
        var input = e.target;
        var val = input.value;
        var end = input.selectionEnd;
        if (e.keyCode == 32 && (val[end - 1] == " " || val[end] == " ")) {
            e.preventDefault();
            return false;
        }
    });
}

const sameAsAddress = document.createElement('div');
sameAsAddress.className = "sameAsAddress"
sameAsAddress.innerHTML = `<input type = "checkbox"  id = "sameAddrId" onchange="sameAsFunction()"> Permanent address same as communication address`
containerId.appendChild(sameAsAddress);

const pincodeContainer = document.createElement('div');
pincodeContainer.className = "pincodeContainer"
pincodeContainer.innerHTML = `<br>Pincode : <span class="starStyle">*</span><br> <input type="text" class="pincodeClass" id="pincodeId"  onkeypress="errorMessageRemove(event)" maxlength="6" onkeydown="return numericOnly(event);"> <br><span class="error" id="errorpincode"></span><br>`
containerId.appendChild(pincodeContainer);

const submitContainer = document.createElement('div');
submitContainer.className = "submitContainer"
submitContainer.innerHTML = `<button type="submit" onclick="register(event,-1)" class="registerClass" id="registerId">Register</button>
<button type="submit" onclick="cancel()" class="cancelClass">Cancel</button>`
containerId.appendChild(submitContainer);

//auto fill permanenet address
function sameAsFunction() {
    let data = document.getElementById('sameAddrId').checked;
    document.getElementById("permanentAddr").disabled = true;


    if (data == true) {

        document.getElementById("permanentAddr").value = document.getElementById("communicationAddr").value;

    } else {
        document.getElementById("permanentAddr").value = "";
        document.getElementById("permanentAddr").disabled = false;


    }
}

communicationAddr.addEventListener('input', sameAsFunction);

let imageSrc = document.getElementById('uploadImageId').src;
// console.log(imageSrc + "image");
selectData();
let genderCheck = document.getElementsByName('gender');


//gender remove error onchange
for (i = 0; i < genderCheck.length; i++) {

    genderCheck[i].onchange = function() {
        document.getElementById('errorgender').innerHTML = "";
    }
}



function register(event, indexValue) {
    event.preventDefault();
    let errorOraganizationName = document.getElementById('errorOraganizationName'),
        errorfirstName = document.getElementById('errorfirstName'),
        errorlastName = document.getElementById('errorlastName'),
        errorgender = document.getElementById('errorgender'),
        errordob = document.getElementById('errordob'),
        errormobile = document.getElementById('errormobile'),
        errormail = document.getElementById('errormail'),
        errorcountry = document.getElementById('errorcountry'),
        errorstate = document.getElementById('errorstate'),
        errorcity = document.getElementById('errorcity'),
        errorcommunication = document.getElementById('errorcommunication'),
        errorpermanent = document.getElementById('errorpermanent'),
        errorpincode = document.getElementById('errorpincode');

    errorOraganizationName.innerHTML = "";
    errorfirstName.innerHTML = "";
    errorlastName.innerHTML = "";
    errorgender.innerHTML = "";
    errordob.innerHTML = "";
    errormobile.innerHTML = "";
    errorpincode.innerHTML = "";
    errormail.innerHTML = "";
    errorcountry.innerHTML = "";
    errorstate.innerHTML = "";
    errorcity.innerHTML = "";
    errorcommunication.innerHTML = "";
    errorpermanent.innerHTML = "";

    const select = document.getElementById('myselect');
    const option = select.options[select.selectedIndex];
    const organizationValue = option.text;


    let firstName = document.getElementById('firstNameinputId').value;
    let lastName = document.getElementById('lastNameinputId').value;
    let genderCheck = document.getElementsByName('gender');

    let userdob = document.getElementById('dobId').value;
    let usermobileNumberId = document.getElementById('mobileNumberId').value;
    let usermailId = document.getElementById('mailId').value;
    const countryName = document.getElementById('countryName');
    const stateName = document.getElementById('stateName');
    let usercityId = document.getElementById('cityId').value;
    let usercommunicationAddress = document.getElementById('communicationAddr').value;
    let userpermanentAddress = document.getElementById('permanentAddr').value;
    let userpincode = document.getElementById('pincodeId').value;
    for (i = 0; i < genderCheck.length; i++) {
        if (genderCheck[i].checked)

            genderSelect = genderCheck[i].value
    }
    const countryNameoption = countryName.options[countryName.selectedIndex];
    const countryNameValue = countryNameoption.text;


    const stateNameoption = stateName.options[stateName.selectedIndex];
    const stateNameValue = stateNameoption.text;


    if (organizationValue === 'Select organization' || !firstName || !lastName || !genderSelect || !userdob ||
        !usermobileNumberId || !usermailId || countryNameValue === 'Select Country' || stateNameValue === 'Please select Country first' ||
        !usercityId || !usercommunicationAddress || !userpermanentAddress || !userpincode) {
        if (organizationValue === 'Select organization') {

            errorOraganizationName.innerHTML = "Please select organization field";
        }
        if (!firstName) {
            errorfirstName.innerHTML = "Please enter first name";
        }
        if (!lastName) {
            errorlastName.innerHTML = "Please enter last Name";
        }
        if (!genderSelect) {
            errorgender.innerHTML = "Please gender option select";
        }
        if (!userdob) {
            errordob.innerHTML = "Please enter date of birth";
        }
        if (!usermobileNumberId) {
            errormobile.innerHTML = "Please enter mobile number";
        }
        if (!usermailId) {
            errormail.innerHTML = "Please enter email Id";
        }
        if (countryNameValue === 'Select Country') {
            errorcountry.innerHTML = "Please enter country name";
        }
        if (stateNameValue === 'Please select Country first') {
            errorstate.innerHTML = "Please enter state name";
        }
        if (!usercityId) {
            errorcity.innerHTML = "Please enter city name";
        }
        if (!usercommunicationAddress) {
            errorcommunication.innerHTML = "Please enter communication address";
        }
        if (!userpermanentAddress) {

            errorpermanent.innerHTML = "Please enter permanent address";
        }
        if (!userpincode) {
            errorpincode.innerHTML = "Please enter pincode";
        }

    } else {
        if (usermailId) {
            console.log(usermailId)
            let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            console.log(mailformat.test(usermailId))
            if (mailformat.test(usermailId)) {
                document.getElementById('errormail').innerHTML = "";
                let imageSrc = document.getElementById('uploadImageId').src;
                console.log(imageSrc);
                let fullname = firstName.trim() + " " + lastName.trim();
                formDataDetail = {
                    image: profileImageUrl,
                    organization: organizationValue,
                    name: fullname,
                    gender: genderSelect,
                    dob: userdob,
                    mobile: usermobileNumberId,
                    mailId: usermailId,
                    countryName: countryNameValue,
                    stateName: stateNameValue,
                    city: usercityId.trim(),
                    communicationAddress: usercommunicationAddress.trim(),
                    permanentAddress: userpermanentAddress.trim(),
                    pincode: userpincode.trim()
                };
                if (indexValue === -1) {
                    let arr = getCrudData() || [];
                    arr.push(formDataDetail);
                    localStorage.setItem("crud", JSON.stringify(arr));
                    selectData();
                    document.getElementById("resetForm").reset();
                } else {
                    let arr = getCrudData() || [];
                    arr.splice(indexValue, 1, formDataDetail);
                    indexValue = -1;
                    document.getElementById('registerId').innerHTML = "Register";
                    localStorage.setItem("crud", JSON.stringify(arr));
                    selectData();
                    document.getElementById("resetForm").reset();
                }




                // console.log(deleteData(event));
                // document.getElementById('registerId').innerHTML = "Register";
                // document.getElementById("resetForm").reset();

            } else {
                document.getElementById('errormail').innerHTML = "please enter valid email address!";
            }
        }
        let deleteBtn = document.getElementsByClassName("deleteBtn").length;
        console.log(deleteBtn);
        for (i = 0; i < deleteBtn; i++) {
            document.getElementsByClassName("deleteBtn")[i].style.pointerEvents = "auto";
        }


        let editBtn = document.getElementsByClassName("editBtn").length;

        for (i = 0; i < editBtn; i++) {
            document.getElementsByClassName("editBtn")[i].style.pointerEvents = "auto";
        }
        document.getElementById("permanentAddr").disabled = false;
        document.getElementById('profileImage').style.display = "none";
        profileimage.src = '';

    }

}



function selectData() {

    let getDataValue = getCrudData()

    if (getDataValue != null) {
        let html = '';
        let sno = 1;

        for (let i = 0; i < getDataValue.length; i++) {
            html = html + `<tr><td>${sno}</td><td>${getDataValue[i].name}</td><td>${getDataValue[i].gender}</td><td>${getDataValue[i].dob}</td><td>${getDataValue[i].mobile}</td><td>${getDataValue[i].mailId}</td><td><a href="javascript:void(0)" onclick="editData(${i})" class="editBtn">Edit</a>/<a href="javascript:void(0)" onclick="deleteData(${i})" class="deleteBtn">Delete</a></td></tr>`;
            sno++;
        }
        document.getElementById('root').innerHTML = html;
    }
}

function getCrudData() {
    let arr = JSON.parse(localStorage.getItem('crud'));
    return arr;
}

function editData(rid) {
    let deleteBtn = document.getElementsByClassName("deleteBtn").length;
    console.log(deleteBtn);
    for (i = 0; i < deleteBtn; i++) {
        document.getElementsByClassName("deleteBtn")[i].style.pointerEvents = "none";
    }


    let editBtn = document.getElementsByClassName("editBtn").length;

    for (i = 0; i < editBtn; i++) {
        document.getElementsByClassName("editBtn")[i].style.pointerEvents = "none";
    }
    id = rid;
    let arr = getCrudData();
    console.log(arr[rid].image)
    document.getElementById('profileImage').src = arr[rid].image;
    document.getElementById('profileImage').style.display = "block";
    document.getElementById('myselect').value = arr[rid].organization;
    document.getElementById('countryName').value = arr[rid].countryName;
    countryName = arr[rid].countryName;

    stateName.length = 1; // remove all options bar first

    if (this.selectedIndex < 1) return; // done 
    let state = stateObject[countryName];

    for (let i = 0; i < state.length; i++) {


        stateName.options[stateName.options.length] = new Option(state[i], state[i]);

    }


    document.getElementById('stateName').value = arr[rid].stateName;
    console.log(arr[rid].organization)

    let name = arr[rid].name.split(" ");

    document.getElementById('firstNameinputId').value = name[0];
    document.getElementById('lastNameinputId').value = name[1];
    console.log(arr[rid].gender)
    if (arr[rid].gender == 'Male')
        document.getElementById('male').checked = arr[rid].gender;
    else {
        document.getElementById('female').checked = arr[rid].gender;
    }

    document.getElementById('dobId').value = arr[rid].dob;

    document.getElementById('mobileNumberId').value = arr[rid].mobile;


    document.getElementById('mailId').value = arr[rid].mailId;

    document.getElementById('cityId').value = arr[rid].city;


    document.getElementById('communicationAddr').value = arr[rid].communicationAddress;

    document.getElementById('permanentAddr').value = arr[rid].permanentAddress;
    if (arr[rid].communicationAddress === arr[rid].permanentAddress) {
        document.getElementById('sameAddrId').checked = true;
        document.getElementById('permanentAddr').disabled = true;
    }

    document.getElementById('pincodeId').value = arr[rid].pincode;
    document.getElementById('registerId').innerHTML = "Update";
    document.getElementById('registerId').onclick = (e) => {

        register(e, rid);

        document.getElementById("resetForm").reset();
    }



}

function deleteData(rid) {
    let arr = getCrudData();
    arr.splice(rid, 1);
    setCrudData(arr);
    selectData();
}

function setCrudData(arr) {
    localStorage.setItem('crud', JSON.stringify(arr));
}


function cancel() {

    document.getElementById('profileImage').style.display = "none";
    profileimage.src = '';
    document.getElementById('registerId').innerHTML = "Register";

    let deleteBtn = document.getElementsByClassName("deleteBtn").length;
    console.log(deleteBtn);
    for (i = 0; i < deleteBtn; i++) {
        document.getElementsByClassName("deleteBtn")[i].style.pointerEvents = "auto";
    }


    let editBtn = document.getElementsByClassName("editBtn").length;

    for (i = 0; i < editBtn; i++) {
        document.getElementsByClassName("editBtn")[i].style.pointerEvents = "auto";
    }
    document.getElementById("resetForm").reset();
}