function cardValidate(ele) {
    let cardNo = ele.value;
    let masterCardRegex = /^(?:5[1-5][0-9]{14})$/;
    let visaCardRegex = /^(?:4[0-9]{12})(?:[0-9]{3})$/;
    let americanExpCardRegex = /^(?:3[47][0-9]{13})$/;

    let cardName = "-";
    if (masterCardRegex.test(cardNo)) {
        cardName = "Master Card";
    } else if (visaCardRegex.test(cardNo)) {
        cardName = "Visa Card";
    } else if (americanExpCardRegex.test(cardNo)) {
        cardName = "American Express Card";
    }
    document.querySelector("#out").innerText = cardName;
}