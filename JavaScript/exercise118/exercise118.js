let row = 5;
let column = 5;

function countPaths() {
    let maze = [
        [0, 0, 0, -1, 0],
        [0, -1, 0, -1, 0],
        [-1, 0, 0, -1, 0],
        [0, 0, 0, -1, 0],
        [0, 0, 0, -1, 0]
    ];

    if (maze[0][0] === -1)
        return false;

    for (let i = 0; i < row; i++) {
        if (maze[i][0] === 0)
            maze[i][0] = 1;

        else
            break;
    }

    for (let i = 1; i < column; i++) {
        if (maze[0][i] === 0)
            maze[0][i] = 1;

        else
            break;
    }

    for (let i = 1; i < row; i++) {
        for (let j = 1; j < column; j++) {

            if (maze[i][j] === -1)
                continue;

            if (maze[i - 1][j] > 0)
                maze[i][j] = (maze[i][j] + maze[i - 1][j]);

            if (maze[i][j - 1] > 0)
                maze[i][j] = (maze[i][j] + maze[i][j - 1]);
        }
    }
    document.writeln("The No.of ways moving start to end : " + maze[row - 1][column - 1] + "<br>" + "The condition is : ")
    return (maze[row - 1][column - 1] > 0) ? true : false;

}




document.write(countPaths());