let min = 1,
    sec = 60;
let playerScoreSum = 0;
let cpuScoreSum = 0;
const cards = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "S", "R", "+2"];
const color = ["red", "green", "blue", "orange"];
const specialCards = ["0", "W", "+4"]
let num;
const randomColor = [];
const cpuCards = [];
const playerCards = [];
const deck = [];
const randomCards = [];
let drawCard = [];
let i;
let deckCard = [];
const playerCardId = document.getElementById('Player');
let sum = 0;
let unoValue = document.getElementById("Uno");
let unoClicked = false;


//to push card color and value in deck
for (let x = 0; x < color.length; x++) {
    for (let i = 0; i < cards.length; i++) {

        let card = { Color: color[x], Card: cards[i] };
        deck.push(card, card);
    }
    for (let j = 0; j < specialCards.length; j++) {
        let card = { Color: color[x], Card: specialCards[j] };
        deck.push(card);
    }


}

//shuffle cards
for (let i = 0; i < deck.length; i++)
    randomCards.push(deck[Math.floor((Math.random() * deck.length))]);



for (let i = 0; i < 14; i++) {
    if (i % 2 !== 0) {
        playerCards.push(randomCards[i]);
        playerCardDivFunction(i);


    } else {
        cpuCards.push(randomCards[i]);
        cpuCardDivFunction(i);

    }

}
//open card to the deck
deckCard = randomCards[14];

document.getElementById('deckCard').innerHTML = deckCard.Card;
document.getElementById('deckCard').style.background = deckCard.Color;

//remaining cards to store draw card
drawCard = randomCards.slice(15, 108);

//creating div from cpuCards
function cpuCardDivFunction(i) {
    const cpuCardId = document.getElementById('cpuPlayerId');
    const cpuCardDivCreate = document.createElement('div');
    cpuCardDivCreate.className = "cpudivClass"
    cpuCardDivCreate.innerHTML = "UNO";
    cpuCardId.appendChild(cpuCardDivCreate);
}
//creating div from playerCards
function playerCardDivFunction(i) {

    const playerCardDivCreate = document.createElement('div');
    playerCardDivCreate.className = "innerCard playerCardClass ";
    playerCardDivCreate.innerHTML = randomCards[i].Card;
    playerCardDivCreate.style.background = randomCards[i].Color;
    playerCardId.appendChild(playerCardDivCreate);
}

playerdeckCardFunction()





function playerdeckCardFunction() {
    unoClick();

    win();


    const cpuIcon = document.getElementById('cpuIcon');
    cpuIcon.innerHTML = '';
    const playerIcon = document.getElementById('playerIcon');
    playerIcon.innerHTML = '<i class="fa fa-hand-o-right"></i>';

    let player = document.getElementById('Player');


    player.style.cursor = 'pointer';
    for (let i = 0; i < playerCards.length; i++) {

        player.children[i].onclick = function() {

            let playerCardMatch = playerCards[i];
            //wildCard Function
            function wildCardFunction(i) {
                player.children[i].remove();
                playerCards.splice(i, 1);
                const wildCardContainer = document.getElementById('wildCardContainer');
                wildCardContainer.style.cursor = 'pointer'
                wildCardContainer.innerHTML = `<div id="playerWildCardContainer"></div>`

            }
            if (playerCardMatch.Card === 'W') {
                wildCardFunction(i);
                for (let i = 0; i < color.length; i++) {
                    const playerWildCardContainer = document.getElementById('playerWildCardContainer');
                    const wildCardColor = document.createElement('div');
                    wildCardColor.className = 'wildCardColor';
                    wildCardColor.style.background = color[i];

                    playerWildCardContainer.appendChild(wildCardColor);
                    playerWildCardContainer.children[i].onclick = function() {
                        document.getElementById('deckCard').style.background = color[i];
                        deckCard.Color = color[i];
                        wildCardContainer.removeChild(playerWildCardContainer);
                        player.style.background = "";
                        setTimeout(playCpu, 1000);
                        playerdeckCardFunction();

                    }

                }

            }
            // +4 special card 
            else if (playerCardMatch.Card === '+4') {
                wildCardFunction(i);
                for (let i = 0; i < 4; i++) {
                    cpuCardDivFunction(i);
                    cpuCards.push(drawCard[i]);
                }
                drawCard.splice(0, 4);
                for (let i = 0; i < color.length; i++) {
                    const playerWildCardContainer = document.getElementById('playerWildCardContainer');
                    const wildCardColor = document.createElement('div');
                    wildCardColor.className = 'wildCardColor';
                    wildCardColor.style.background = color[i];

                    playerWildCardContainer.appendChild(wildCardColor);
                    playerWildCardContainer.children[i].onclick = function() {
                        document.getElementById('deckCard').style.background = color[i];
                        deckCard.Color = color[i];
                        wildCardContainer.removeChild(playerWildCardContainer);

                        // playerdeckCardFunction();

                    }

                }
                playerdeckCardFunction();
            } else if (playerCardMatch.Color === deckCard.Color || playerCardMatch.Card === deckCard.Card) {
                deckCard = playerCardMatch;
                document.getElementById('deckCard').innerHTML = playerCardMatch.Card;
                document.getElementById('deckCard').style.background = playerCardMatch.Color;
                player.children[i].remove();
                playerCards.splice(i, 1);
                if ((deckCard.Card === 'R') || (deckCard.Card === 'S')) {

                    playerdeckCardFunction();
                } else if (deckCard.Card === '+2') {
                    for (let i = 0; i < 2; i++) {
                        cpuCardDivFunction(i);
                        cpuCards.push(drawCard[i]);

                    }
                    drawCard.splice(0, 2);

                    playerdeckCardFunction();

                } else {

                    setTimeout(playCpu, 1000);
                    playerdeckCardFunction();
                }
            }


        }

    }





}


console.log(cpuCards);
console.log(playerCards);
console.log(randomCards);

function drawCards() {
    const playerCardId = document.getElementById('Player');
    const playerCardDivCreate = document.createElement('div');
    playerCardDivCreate.className = "innerCard playerCardClass ";
    playerCardDivCreate.innerHTML = drawCard[0].Card;
    playerCardDivCreate.style.background = drawCard[0].Color;
    playerCards.push(drawCard[0]);
    playerCardId.appendChild(playerCardDivCreate);
    drawCard.splice(0, 1);
    setTimeout(playCpu, 5000);
    playerdeckCardFunction();

    unoClicked = 0;

}


function playCpu() {

    const playerIcon = document.getElementById('playerIcon');
    playerIcon.innerHTML = '';
    const cpuIcon = document.getElementById('cpuIcon');
    cpuIcon.innerHTML = '<i class="fa fa-hand-o-left"></i>';
    setTimeout(() => {


        unoClick();

        win();




        const cpuPlayer = document.getElementById('cpuPlayerId');

        for (let i = 0; i < cpuCards.length; i++) {
            let cpuCardMatch = cpuCards[i];

            function cpuWildCardFunction(i) {
                cpuPlayer.children[i].remove();
                cpuCards.splice(i, 1);
                const wildCardContainer = document.getElementById('wildCardContainer');
                wildCardContainer.style.cursor = 'pointer'
                wildCardContainer.innerHTML = `<div id="playerWildCardContainer"></div>`
            }
            console.log(cpuCardMatch.Card, cpuCardMatch.Color, deckCard.Card, deckCard.Color)
            if (cpuCardMatch.Card === 'W') {
                // window.alert('Wild card will be selected and to change the color of the deckcard')
                cpuWildCardFunction(i);
                for (let j = 0; j < color.length; j++) {
                    const playerWildCardContainer = document.getElementById('playerWildCardContainer');
                    let cpuwildCardColor = document.createElement('div');
                    cpuwildCardColor.className = 'wildCardColor';
                    cpuwildCardColor.style.background = color[j];
                    playerWildCardContainer.appendChild(cpuwildCardColor);
                    for (let k = 0; k < color.length; k++) {
                        randomColor.push(color[Math.floor((Math.random() * color.length))]);
                    }
                }
                console.log(deckCard.Color, randomColor[0])
                document.getElementById('deckCard').style.background = randomColor[0];
                deckCard.Color = randomColor[0];

                wildCardContainer.removeChild(playerWildCardContainer);

                playerdeckCardFunction();
                break;

            } else if (cpuCardMatch.Card === '+4') {
                // window.alert('+4 will be selected and to change the color of the deckcard and add 4 cards to the player');
                cpuWildCardFunction(i);
                for (let j = 0; j < color.length; j++) {
                    const playerWildCardContainer = document.getElementById('playerWildCardContainer');
                    let cpuwildCardColor = document.createElement('div');
                    cpuwildCardColor.className = 'wildCardColor';
                    cpuwildCardColor.style.background = color[j];
                    playerWildCardContainer.appendChild(cpuwildCardColor);
                    for (let k = 0; k < color.length; k++) {
                        randomColor.push(color[Math.floor((Math.random() * color.length))]);
                    }
                }
                console.log(deckCard.Color, randomColor[0])
                document.getElementById('deckCard').style.background = randomColor[0];
                deckCard.Color = randomColor[0];

                wildCardContainer.removeChild(playerWildCardContainer);

                for (let i = 0; i < 4; i++) {
                    const playerCardDivCreate = document.createElement('div');
                    playerCardDivCreate.className = "innerCard playerCardClass ";
                    playerCardDivCreate.innerHTML = drawCard[i].Card;
                    playerCardDivCreate.style.background = drawCard[i].Color;
                    playerCardId.appendChild(playerCardDivCreate);
                    playerCards.push(drawCard[i]);

                }

                drawCard.splice(0, 4);

                setTimeout(playCpu, 2000);
                // playerdeckCardFunction();
                break;

            } else if (cpuCardMatch.Color === deckCard.Color || cpuCardMatch.Card === deckCard.Card) {
                deckCard = cpuCardMatch;
                document.getElementById('deckCard').innerHTML = cpuCardMatch.Card;
                document.getElementById('deckCard').style.background = cpuCardMatch.Color;
                cpuPlayer.children[i].remove();
                cpuCards.splice(i, 1);

                if ((cpuCardMatch.Card === 'R') || (cpuCardMatch.Card === 'S')) {
                    // window.alert('Once again repeat cpu deck')

                    setTimeout(playCpu, 2000);
                    break;

                } else if (cpuCardMatch.Card === '+2') {
                    // window.alert('+2 will be selected and to change the color of the deckcard and add 2 cards to the player')
                    for (let i = 0; i < 2; i++) {
                        const playerCardDivCreate = document.createElement('div');
                        playerCardDivCreate.className = "innerCard playerCardClass ";
                        playerCardDivCreate.innerHTML = drawCard[i].Card;
                        playerCardDivCreate.style.background = drawCard[i].Color;
                        playerCardId.appendChild(playerCardDivCreate);
                        playerCards.push(drawCard[i]);

                    }

                    drawCard.splice(0, 2);

                    setTimeout(playCpu, 2000);
                    // playerdeckCardFunction();
                    break;

                } else {

                    playerdeckCardFunction();
                    break;
                }





            } else if (i === cpuPlayer.children.length - 1) {
                const cpuPlayerId = document.getElementById('cpuPlayerId');
                const cpuCardDivCreate = document.createElement('div');
                cpuCardDivCreate.className = "cpudivClass"
                cpuCardDivCreate.innerHTML = "UNO";
                cpuCards.push(drawCard[0]);

                cpuPlayerId.appendChild(cpuCardDivCreate);
                drawCard.splice(0, 1);

                playerdeckCardFunction();
                break;
            }


        }
    }, 2000);
}




function win() {

    if (playerCards.length === 0) {
        for (i = 0; i < cpuCards.length; i++) {
            playerWin(i);


            console.log(sum);
            const resultScore = document.getElementById('resultScore');
            const score = document.createElement('div');
            score.className = "score";
            score.innerHTML = "The player Score is : " + sum;
            resultScore.appendChild(score);
        }


    } else if (cpuCards.length == 0) {
        for (i = 0; i < playerCards.length; i++) {
            cpuWin(i);
        }
        console.log(sum);
        const resultScore = document.getElementById('resultScore');
        const score = document.createElement('div');
        score.className = "score";
        score.innerHTML = "The CPU Score is : " + sum;
        resultScore.appendChild(score);
        // window.alert("Cpu Score : " + sum);
        // playCpu();


    }


}

function playerWin(i) {


    if (cpuCards[i].Card === "R" || cpuCards[i].Card === "S" || cpuCards[i].Card === '+2') {
        sum += 20;

    } else if (cpuCards[i].Card === "W" || cpuCards[i].Card === "+4") {
        sum += 50;


    } else {
        let num = cpuCards[i].Card;
        switchCard(num);


    }
}




function cpuWin(i) {


    if (playerCards[i].Card === "R" || playerCards[i].Card === "S" || playerCards[i].Card === '+2') {
        sum += 20;

    } else if (playerCards[i].Card === "W" || playerCards[i].Card === "+4") {
        sum += 50;


    } else {
        let num = playerCards[i].Card;
        switchCard(num);
    }
}



function switchCard(num) {
    switch (num) {
        case "0":
            sum += 0;
            break;
        case "1":
            sum += 1;
            break;
        case "2":
            sum += 2;
            break;
        case "3":
            sum += 3;
            break;
        case "4":
            sum += 4;
            break;
        case "5":
            sum += 5;
            break;
        case "6":
            sum += 6;
            break;
        case "7":
            sum += 7;
            break;
        case "8":
            sum += 8;
            break;
        case "9":
            sum += 9;
            break;
    }
}

function Uno() {
    unoClicked = true;

    if (playerCards.length === 2) {
        if (unoClicked === true) {
            document.getElementById('Uno').style.background = 'red';
            document.getElementById('Uno').style.color = 'white';

        }
    }


}

function unoClick() {
    if (playerCards.length === 2) {
        setTimeout(function() {
            if (unoClicked === false) {

                for (let i = 0; i < 2; i++) {

                    const playerCardDivCreate = document.createElement('div');
                    playerCardDivCreate.className = "innerCard playerCardClass ";
                    playerCardDivCreate.innerHTML = drawCard[i].Card;
                    playerCardDivCreate.style.background = drawCard[i].Color;
                    playerCardId.appendChild(playerCardDivCreate);
                    playerCards.push(drawCard[i]);


                }
                drawCard.splice(0, 2);
                console.log(playerCards);
                playerdeckCardFunction();



            }
        }, 5000);


    } else {
        document.getElementById('Uno').style.background = 'white';
        document.getElementById('Uno').style.color = 'black';
    }

    unoClicked = false;

}

function minutes() {


    document.getElementById("timer").innerHTML = `0${min} : ${sec}`;
    if (min === 0) {
        document.getElementById("timer").innerHTML = `00 : 00`;
        timerResult();
        clearInterval(minInterval);
        clearInterval(secInterval);
    }
    min--;
}

function seconds() {
    sec--;

    if (sec <= 0) sec = 60;
    if (sec < 10)
        (document.getElementById("timer").innerHTML = `0${min} : 0${sec}`);
    document.getElementById("timer").innerHTML = `0${min} : ${sec}`;

}
minInterval = setInterval(minutes, 60000);
secInterval = setInterval(seconds, 1000);

function timerResult() {
    for (i = 0; i < cpuCards.length; i++) {
        playerWin(i);
        playerScoreSum = sum;
    }
    for (i = 0; i < playerCards.length; i++) {
        cpuWin(i);
        cpuScoreSum = sum;
    }



    if (playerScoreSum < cpuScoreSum) {
        const resultScore = document.getElementById('resultScore');
        const score = document.createElement('div');
        score.className = "score";
        score.innerHTML = "The player Score is : " + playerScoreSum + "<br> The Player is Winner";
        resultScore.appendChild(score);

    } else {
        const resultScore = document.getElementById('resultScore');
        const score = document.createElement('div');
        score.className = "score";
        score.innerHTML = "The CPU Score is : " + cpuScoreSum + "<br> The CPU is Winner";
        resultScore.appendChild(score);
    }

}