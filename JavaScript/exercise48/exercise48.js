function findVowel() {
    let stringName = document.getElementById('stringName').value;
    stringName = stringName.toUpperCase()
    let acceptableLetters = ['A', 'E', 'I', 'O', 'U'];


    let checkString = (chr) => {
        let test = acceptableLetters.some((letter) => letter === chr)
        return test


    }

    for (let i = 0; i < stringName.length; i++) {
        let chr = stringName.charAt(i);
        let condition = checkString(chr);

        if (condition) {
            console.log(stringName.indexOf(chr));
            break;
        }

    }

}

findVowel()