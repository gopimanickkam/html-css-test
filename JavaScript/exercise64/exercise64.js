function reverse1() {
    let str = document.getElementById('inputString').value;
    let r = "";
    for (let i = str.length - 1; i >= 0; i--) {
        r += str.charAt(i);
    }
    document.getElementById('result').innerHTML = "The given string converted to reverse " + r;
}