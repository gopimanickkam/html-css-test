let arr = [];

function largestArr() {

    let inputValues = document.getElementById('inputValues').value;
    arr.push(inputValues);
    let maxi = arr[0];
    for (let i = 1; i < arr.length; i++) {
        if (arr[i] > maxi)
            maxi = arr[i];
    }
    document.getElementById('largestNumber').innerHTML = "Largest in given array is " + maxi;

}