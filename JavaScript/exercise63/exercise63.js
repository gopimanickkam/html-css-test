function fact() {
    let n = document.getElementById("num1").value;
    let answer = 1;
    if (n == 0 || n == 1) {
        return document.getElementById('result1').innerHTML = "The factorial of given number is : " + answer;
    }
    for (let i = n; i >= 1; i--) {
        answer = answer * i;
    }
    document.getElementById('result1').innerHTML = "The factorial of given number is : " + answer;

}

function fact2(num) {
    if (num == 0) {
        return 1;
    } else {
        return num * fact2(num - 1);
    }
}

function fact1() {
    var num = document.getElementById("number").value;
    var f = fact2(num);
    document.getElementById("res").innerHTML = "The factorial of the number " + num + " is: " + f;
}