function isObject(value1) {
    if (typeof(value1) === "object" && !(Array.isArray(value1)))
        return console.log("The given value is object");
    console.log("The given value is not object")

    // let datatypeObject = typeof value1;

    // return datatypeObject === 'function' || datatypeObject === 'object' && !!value1;
}

console.log(isObject({ name: 'Robert' }));

console.log(isObject('bar'));

console.log(isObject([1, 2]));