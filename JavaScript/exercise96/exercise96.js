function is_regexp(value1) {
    return toString.call(value1) === '[object RegExp]';
}

console.log(is_regexp(/test/));

console.log(is_regexp([1, 2, 3]));

console.log(is_regexp(null));