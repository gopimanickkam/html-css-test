function validEmail() {
    let emailString = document.getElementById('emailString').value;
    let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (mailformat.test(emailString)) {
        return document.getElementById('result').innerHTML = "Valid email address!";
    }
    document.getElementById('result').innerHTML = "You have entered an invalid email address!";

}