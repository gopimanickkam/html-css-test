function isJson(value1) {
    return toString.call(value1) === '[object Object]';
}

console.log(isJson({ 'name': 'Clark Kent', 'Interest': ['cricket', 'volleyBall', 'football'], 'age': 27, 'academics': { 'English': 100, 'Math': 100, }, }))