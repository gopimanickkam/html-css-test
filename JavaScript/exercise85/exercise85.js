function testFairness() {
    let agatha = [
            [4, 3],
            [2, 4],
            [1, 2]
        ],
        bertha = [
            [6, 2],
            [4, 2],
            [1, 1],
            [1, 1]
        ];
    let aCount = 0;
    let bCount = 0;
    for (let i = 0; i < agatha.length; i++) {
        let chocolateAmount = agatha[i][0] * agatha[i][1];
        aCount += chocolateAmount;
    }
    for (let i = 0; i < bertha.length; i++) {
        let chocolateAmount = bertha[i][0] * bertha[i][1];
        bCount += chocolateAmount;
    }

    return document.getElementById('result').innerHTML = (aCount === bCount);
}