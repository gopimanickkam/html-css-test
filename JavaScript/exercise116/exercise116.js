let dictionary = [];
let i, j, k = 0,
    length1 = 0;


function addArray() {

    let dictionaryWord = document.getElementById('dictionaryWord').value.toUpperCase();
    dictionary.push(dictionaryWord);
    document.getElementById('dictionary').innerHTML = JSON.stringify(dictionary);
}

function findWords() {
    let boggle = [
        ["F", "K", "L", "M", "N", "T", "O", "P"],
        ["Q", "R", "S", "F", "U", "I", "W", "X"],
        ["Y", "Z", "H", "I", "T", "M", "L", "O"],
        ["T", "Q", "R", "H", "I", "E", "T", "F"],
        ["M", "N", "L", "K", "E", "J", "T", "O"],
        ["R", "I", "A", "P", "Q", "L", "F", "K"],
        ["C", "T", "C", "L", "F", "K", "L", "O"],
        ["A", "C", "D", "F", "L", "M", "N", "O"]
    ];
    let foundWords = [],
        notFoundWords = [];
    for (i = 0; i <= dictionary.length; i++) {
        findWord(dictionary[i]);
        document.getElementById('foundWords').innerHTML = "The following matching words in 8x8 grid is : " + foundWords;
        document.getElementById('notFoundWords').innerHTML = "The following not matching words in 8x8 grid is: " + notFoundWords;

    }

    function findWord(wordToSearch) {
        const wordLength = wordToSearch.length;
        const firstLetterToSearch = wordToSearch[0];
        for (i = 0; i < boggle.length; i++) {
            const currentArray = boggle[i]
            const arrayIndexOfFirstLetter = currentArray.indexOf(firstLetterToSearch)
                //horizontal direction
            if (currentArray.length - arrayIndexOfFirstLetter >= wordLength) {

                const possibleWordArrayFound = currentArray.slice(arrayIndexOfFirstLetter, arrayIndexOfFirstLetter + wordLength)

                if (possibleWordArrayFound.join('') === wordToSearch) {
                    return foundWords += wordToSearch + ",";
                }

            }
            //vertical direction
            if (boggle.length - i >= wordLength) {
                let possibleWordArrayFound = []
                for (let j = 0; j < wordLength; j++) {
                    possibleWordArrayFound.push(boggle[i + j][arrayIndexOfFirstLetter])
                }
                if (possibleWordArrayFound.join('') === wordToSearch) {
                    return foundWords += wordToSearch + ",";
                }

            }

        }
        let revString = "";
        for (let rev = wordToSearch.length - 1; rev >= 0; rev--) {
            revString += wordToSearch[rev];
        }
        for (i = 0; i < boggle.length; i++) {
            let firstLetterToSearch = revString[0];
            let currentArray = boggle[i];
            const arrayIndexOfFirstLetter = currentArray.indexOf(firstLetterToSearch)


            //horizontal reverse direction
            if (currentArray.length - arrayIndexOfFirstLetter >= wordLength) {

                let possibleWordArrayFound = currentArray.slice(arrayIndexOfFirstLetter, arrayIndexOfFirstLetter + wordLength)

                if (possibleWordArrayFound.join('') === revString) {

                    return foundWords += wordToSearch + ",";
                }

            }

            //vertical reverse direction
            if (boggle.length - i >= wordLength) {


                let possibleWordArrayFound = [];

                for (j = 0; j < wordLength; j++) {
                    possibleWordArrayFound.push(boggle[i + j][arrayIndexOfFirstLetter]);

                    // console.log(i, j, arrayIndexOfFirstLetter);
                }

                if (possibleWordArrayFound.join('') === revString) {
                    return foundWords += wordToSearch + ",";
                }

            }


        }

        //left-bottom diagonal
        for (i = 0; i < boggle.length; i++) {
            for (j = 0; j < boggle.length; j++) {
                if (boggle[i][j] === wordToSearch[k]) {

                    k++;
                    length1++;
                    if (length1 === wordToSearch.length) {
                        return foundWords += wordToSearch + ",";
                    }
                }
            }
        }
        //left-top diagonal
        for (i = boggle.length - 1; i >= 0; i--) {
            for (j = 0; j < boggle.length; j++) {
                if (boggle[i][j] === wordToSearch[k]) {

                    k++;
                    length1++;
                    if (length1 === wordToSearch.length) {
                        return foundWords += wordToSearch + ",";

                    }
                }
            }
        }
        //right-bottom diagonal
        for (i = 0; i <= boggle.length; i++) {
            for (j = boggle.length - 1; j >= 0; j--) {
                if (boggle[i][j] === wordToSearch[k]) {

                    k++;
                    length1++;
                    if (length1 === wordToSearch.length) {
                        return foundWords += wordToSearch + ",";

                    }
                }
            }
        }
        //right-top diagonal
        for (i = boggle.length - 1; i >= 0; i--) {
            for (j = boggle.length; j >= 0; j--) {
                if (boggle[i][j] === wordToSearch[k]) {

                    k++;
                    length1++;
                    if (length1 === wordToSearch.length) {
                        return foundWords += wordToSearch + ",";

                    }
                }
            }
        }

        return notFoundWords += wordToSearch + ",";
    }
}

//