function swap() {
    let num1 = document.getElementById('num1').value;
    let num2 = document.getElementById('num2').value;
    document.getElementById('givenNumber').innerHTML = "The given numbers : " + num1 + " and " + num2;
    let temp;
    temp = num1;
    num1 = num2;
    num2 = temp;
    document.getElementById('swapNumber').innerHTML = "The Swap numbers : " + num1 + " and " + num2;
}