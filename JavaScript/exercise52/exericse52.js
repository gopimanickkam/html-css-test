let arr = [];

function calculate() {

    let inputValues = document.getElementById('inputvalues').value;
    arr.push(inputValues);
    document.getElementById('arrayitems').innerHTML = "Array list : " + arr;


}

function bubbleSort() {

    let len = arr.length,
        i, j;

    for (i = 0; i < len; i++) {
        for (j = 0; j < len - i; j++) {
            if (arr[j] > arr[j + 1]) {
                swap(arr, j, j + 1);
            }

            document.getElementById('result').innerHTML = arr;

        }


    }
}

function swap(arr, first_Index, second_Index) {
    let temp = arr[first_Index];
    arr[first_Index] = arr[second_Index];
    arr[second_Index] = temp;
}