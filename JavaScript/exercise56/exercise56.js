function calculate() {
    let n = document.getElementById('num1').value;
    for (let i = 2; i <= n; i++) {
        let isprime = true;
        for (let j = 2; j <= i / 2; j++) {
            if (i % j == 0) {
                isprime = false;
                break;
            }

        }
        if (isprime) {
            document.getElementById('result').innerHTML = "The Given number is prime";
        } else {
            document.getElementById('result').innerHTML = "The Given number is not prime";
        }
    }
}