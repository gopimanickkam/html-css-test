function stringEndsWith() {
    let str = document.getElementById('str').value;
    let suffix = document.getElementById('suffix').value;
    if (((str === null) || (str === '')) || ((suffix === null) || (suffix === ''))) {
        return document.getElementById('result').innerHTML = false;
    }
    str = str.toString();
    suffix = suffix.toString();

    return document.getElementById('result').innerHTML = (str.indexOf(suffix, str.length - suffix.length) !== -1);
}