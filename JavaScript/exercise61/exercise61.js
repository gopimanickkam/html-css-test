console.log('Armstrong Numbers:');

for (let i = 0; i <= 1000; i++) {


    let sum = 0;


    let temp = i;


    while (temp > 0) {

        let remainder = temp % 10;
        sum += remainder ** 3;

        temp = parseInt(temp / 10);
    }

    if (sum == i) {
        console.log(i);
    }
}