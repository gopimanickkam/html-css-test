function alphabetsOrder() {
    let givenWords = document.getElementById('givenWords').value;
    let sortingOrder = givenWords.split("").sort().join("");
    document.getElementById('sortingOrder').innerHTML = "sorting to given words: " + sortingOrder;
}