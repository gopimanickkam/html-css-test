function multiples() {

    let inputValues = parseInt(document.getElementById("inputValues").value);
    let length = parseInt(document.getElementById("length").value);


    let multipleArray = [];
    let x = 1;
    for (let i = 1; i <= length; i++) {
        multipleArray.push(inputValues * x);
        x++;
    }
    return document.getElementById('result').innerHTML = multipleArray;
}