let array = [];

function addChunkArray() {

    let addArray = parseInt(document.getElementById('addArray').value);
    array.push(addArray);
    document.getElementById('array').innerHTML = JSON.stringify(array);
}

function sumChunkArray() {

    let sum = document.getElementById("perChunkSum").value;
    let total = 0;
    let output = [];

    for (i = 0; i < array.length; i++) {
        for (j = 0; j <= i; j++) {
            if ((array[i] && array[j] === 1) || array[i] != array[j]) {
                // if (array.indexOf(array[i]) != array.indexOf(array[j])) {
                total = parseInt(array[i]) + parseInt(array[j]);
                while (total === parseInt(sum)) {
                    let index = array.indexOf(array[i]);
                    let index1 = array.indexOf(array[j]);
                    let value1 = array.splice(index, 1);
                    let value2 = array.splice(index1, 1);
                    let totalvalue = [...value2, ...value1];
                    output.push(totalvalue);
                    // console.log(array);
                    break;
                }
            }
        }
    }
    document.getElementById('result').innerHTML = JSON.stringify(output);
}