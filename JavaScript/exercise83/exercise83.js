let inputArray = [];
let i, n;

function addArray() {

    let inputValues = parseInt(document.getElementById('inputvalues').value);
    inputArray.push(inputValues);
    document.getElementById('arrayitems').innerHTML = "Array list : " + inputArray;

}


function printArr(inputArray, n) {

    for (i = 0; i < n; i++)
        document.writeln(inputArray[i] + " ");

}

function updateArr() {
    let repeat = document.getElementById('repeat').value;
    n = inputArray.length;


    for (i = 0; i < n; i++) {
        if ((i + 1) % 2 == 1)
            inputArray[i] = inputArray[i] + repeat * 2;

        else
            inputArray[i] = inputArray[i] - repeat * 2;
    }

    printArr(inputArray, n);

}