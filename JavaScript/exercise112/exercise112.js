let chunkArray = [];

function addChunkArray() {

    let addArray = document.getElementById('addArray').value;
    chunkArray.push(addArray);
    document.getElementById('arrayValue').innerHTML = chunkArray;
}

function viewChunkArray() {
    let perChunk = document.getElementById('perChunk').value;

    // let inputArray = chunkArray;

    let result = chunkArray.reduce((resultArray, item, index) => {
        const chunkIndex = Math.floor(index / perChunk)

        if (!resultArray[chunkIndex]) {
            resultArray[chunkIndex] = []
        }

        resultArray[chunkIndex].push(item)

        return resultArray
    }, [])

    console.log(result);
}