function myfunction() {
    let name = document.getElementById("name").value.trim();
    let email = document.getElementById("email").value.trim();
    let city = document.getElementById("city").value.trim();
    let obj = {
        name,
        email,
        city
    };

    // let filled = "";
    // let empty = "";
    // for (let i in obj) {
    //     if (obj[i].length !== 0) {
    //         filled += i + ",";
    //     } else {
    //         empty += i + ",";
    //     }
    // }
    // document.getElementById("return").innerHTML = `the field which are filled :: ${filled}<br> the field which are empty :: ${empty}`;

    let a = Object.entries(obj);
    let filled = [];
    let empty = [];

    for (i = 0; i < a.length; i++) {
        while (a[i][1].length !== 0) {
            filled.push(a[i][0]);
            break;
        }
        while (a[i][1].length === 0) {
            empty.push(a[i][0]);
            break;
        }
    }
    document.getElementById(
        "return"
    ).innerHTML = `the field which are filled :: ${filled}<br> the field which are empty :: ${empty}`;
}