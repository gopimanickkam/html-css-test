Array.prototype.br_search = function(target) {
    let half = parseInt(this.length / 2);
    if (target === this[half]) {
        return half;
    }
    if (target > this[half]) {
        return half + this.slice(half, this.length).br_search(target);
    }

    return this.slice(0, half).br_search(target);

};

l = [0, 1, 2, 3, 4, 7, 6];

console.log(l.br_search(10));