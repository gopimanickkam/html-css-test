function hasKeyValue(obj, key) {
    return obj != null && hasOwnProperty.call(obj, key);
}
console.log(hasKeyValue({
    name: "Clark Kent",
    Interest: ['cricket', 'volleyBall', 'football'],
    age: "27",
    academics: {
        'English': 100,
        'Math': 100,
    }
}, "name"));