let arr = [];

function calculate() {

    let inputValues = document.getElementById('inputvalues').value;
    arr.push(inputValues);
    document.getElementById('arrayitems').innerHTML = "Array list : " + arr;


}

function sumOfSqr() {
    let sum = 0,
        i = arr.length;
    while (i--)
        sum += Math.pow(arr[i], 2);
    document.getElementById('result').innerHTML = " the sum of squares of a numeric vector is " + sum;
}