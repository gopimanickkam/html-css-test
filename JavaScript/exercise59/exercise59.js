function calculate() {
    let num1 = document.getElementById('num1').value;
    let rem, temp, final = 0;


    temp = num1;
    while (num1 > 0) {
        rem = num1 % 10;
        num1 = parseInt(num1 / 10);
        final = final * 10 + rem;
    }
    if (final == temp) {
        document.getElementById('result').innerHTML = "The inputed number is Palindrome";
    } else {
        document.getElementById('result').innerHTML = "The inputed number is not Palindrome";
    }
}