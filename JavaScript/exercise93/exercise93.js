function keyValuePairs(obj) {
    let keys = Object.keys(obj);
    let store = {};
    for (let key of keys) {
        store[obj[key]] = key;
    }
    return store;
}

console.log(keyValuePairs({ title: 'Vhi', author: 'BillGates', book: 'The speed of thought' }))