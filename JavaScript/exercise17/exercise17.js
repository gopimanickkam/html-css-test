function leapYearFunction() {
    const num = document.getElementById('num').value;
    const leapYear = (num % 100 === 0) ? (num % 400 === 0) : (num % 4 === 0);
    document.getElementById('leapYear').innerHTML = leapYear;
}