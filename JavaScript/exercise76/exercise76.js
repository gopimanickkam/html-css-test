function studentObject() {


    let deleteproperty = document.getElementById('deleteproperty').value;
    let studentObj = {
        name: "David Rayy",
        interest: ["cricket", "volleyball", "football"],
        age: 27,
        academics: {
            english: 100,
            maths: 100
        }
    };
    studentObj = removeProp(studentObj, deleteproperty)

    document.getElementById('result').innerHTML = (JSON.stringify(studentObj));

    function removeProp(studentObj, propToDelete) {
        for (let property in studentObj) {
            if (typeof studentObj[property] == "object") {
                delete studentObj.property
                let newJsonData = removeProp(studentObj[property], propToDelete);
                studentObj[property] = newJsonData
            } else if (property === propToDelete) {
                delete studentObj[property];
            }

        }
        return studentObj
    }
}