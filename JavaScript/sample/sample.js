let arr = [];

let locationData = [{ country: "India" }];
let formDataDetail = [];
let profileImageUrl = '';
let genderSelect;
let states = ["Tamil Nadu", "Andhra pradesh", "karnataka", "kerala"]
const genderInput = `Male <input type="radio" id="male" name="gender" value="Male"  onkeypress="errorMessageRemove(event)" required> ` + `Female <input type="radio" id="female" name="gender" onkeypress="errorMessageRemove(event)" value="Female"> `;
const profileContainer = ` <div class="photoContainer">
<div class="photoUploadContainer">
<div class="photo">
<img src="" alt="" id="profileImage"></div>
<div class="photoUploadButton" id = 'photoUploadButtonId'>
</div>
<span class="error" id="errorImage"></span>
</div>

</div>
<div class="organizationContainer">
<div class="organizationName" id='organizationNameId'>Organization Name : <span class="starStyle">*</span><br>
</div><span class="error"  id="errorOraganizationName"></span>
<div class="firstName" id="firstNameId"></div>
</div>
<div class="nameContainer">
<div class="lastName" id="lastNameId"></div>
<div class="gender" id="genderId"></div></div>`;

const organizationData = ['VirdhiTech', 'Google', 'Facebook', 'Whatsapp']
const containerId = document.getElementById('containerId');
const profileContainerDiv = document.createElement('div');
profileContainerDiv.className = "profileContainer"
profileContainerDiv.innerHTML = profileContainer;
containerId.appendChild(profileContainerDiv);
photoUploadButtonId.innerHTML = `<input type="file" class ="uploadButton" id="uploadImageId" accept="image/png, image/jpeg" onchange="changeImg(this)">`


let profileimage = document.getElementById('profileImage');
let uploadButton = document.getElementById('uploadImageId');

function chooseImage() {
    document.getElementById("uploadImageId").click();
}

function changeImg(upload) {
    let file = upload.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(file);
    console.log(reader.result)
    reader.addEventListener("load", function() {
        document.getElementById("profileimage").src = reader.result;
    });

    // document
    //   .getElementById("changeProfile")
    //   .setAttribute("src", window.URL.createObjectURL(upload.files[0]));
}

// uploadButton.addEventListener('change', function() {

//     profileimage.style.display = 'block';
//     profileimage.src = URL.createObjectURL(this.files[0])
//     profileImageUrl = profileimage.src;





//     // console.log({ image: profileImageUrl })

// })

const organizationNameId = document.getElementById('organizationNameId');
let organizationNameIdSelect = document.createElement("SELECT");
organizationNameIdSelect.setAttribute("id", "myselect");
organizationNameId.appendChild(organizationNameIdSelect);

let organizationOption = document.createElement("option");
organizationOption.setAttribute("value", "name");
organizationOption.innerHTML = "Select organization";
document.getElementById("myselect").appendChild(organizationOption);
for (let i = 0; i < organizationData.length; i++) {
    let organizationOption = document.createElement("option");
    organizationOption.setAttribute("value", organizationData[i]);

    organizationOption.innerHTML = organizationData[i];
    document.getElementById("myselect").appendChild(organizationOption);
}
firstNameId.innerHTML = `First Name :  <span class="starStyle">*</span><br> ` + `<input type="text" class="firstNameinput" id="firstNameinputId" onkeypress="errorMessageRemove(event)" onkeydown="return alphaOnly(event);" <br><br> <span class="error" id="errorfirstName"></span>`;
lastNameId.innerHTML = `Last Name :  <span class="starStyle">*</span><br> ` + `<input type="text" class="lastNameinput" id="lastNameinputId"  onkeypress="errorMessageRemove(event)" onkeydown="return alphaOnly(event);" required><br> <span class="error" id="errorlastName"></span>`;
genderId.innerHTML = `Gender :  <span class="starStyle">*</span> <br> ` + genderInput + `<br><span class="error" id="errorgender"></span>`;
// const genderId = document.getElementById('genderId')

function errorMessageRemove(event) {
    document.getElementById(event.target.id).nextElementSibling.nextElementSibling.innerHTML = "";
}

function alphaOnly(event) {
    let key = event.keyCode;
    return ((key >= 65 && key <= 90) || key == 8 || key == 9);
};

function numericOnly(event) {
    let key = event.keyCode;
    return (key >= 48 && key <= 57);

}
const ContactDetailContainerDiv = document.createElement('div');
ContactDetailContainerDiv.className = "ContactDetailContainer";
ContactDetailContainerDiv.innerHTML =
    `<div class="dobContainer" id="dobContainerId">Date of Birth :  <span class="starStyle">*</span><br>
<input type="date" class="dobClass" id="dobId"required  onkeypress="errorMessageRemove(event)" onkeypress="errorMessageRemove(event)"> <br><span class="error" id="errordob" min="2000-01-02"></span>
</div>
<div class="mobileContainer" id="mobileContainerId" onkeypress="errorMessageRemove(event)">Mobile Number :  <span class="starStyle">*</span><br>
<input type="text" class="mobileNumber" id=mobileNumberId name="mobileNumber" maxlength="10" onkeypress="return numericOnly(event);" required ><br><span class="error" id="errormobile"></span>
</div>
<div class="mailIdContainer" id="mailIdContainerId" onkeypress="errorMessageRemove(event)">Email ID:  <span class="starStyle">*</span><br>
<input type="email" class="mailIdClass" id="mailId" required><br><span class="error" id="errormail"></span><br>
</div>`
containerId.appendChild(ContactDetailContainerDiv);

const locationContainer = document.createElement('div');
locationContainer.className = "locationContainer"
locationContainer.innerHTML = `<div><div class="countryContainer " id="countryContainerId">
Country:   <span class="starStyle">*</span> <br> 
  
</div>
<span class="error" id="errorcountry"><br>  
</div>

<div><div class="stateContainer" id="stateContainerId" >
  State:  <span class="starStyle">*</span> <br>
</div>
<span class="error" id="errorstate"><br>
</div>
<div class="cityContainer" id="cityIdContainerId">City : <span class="starStyle">*</span><br>
    <input type="text" class="cityClass" id="cityId"  onkeypress="errorMessageRemove(event)" onkeydown="return alphaOnly(event);"> <br><span class="error" id="errorcity"></span>
</div>`;
containerId.appendChild(locationContainer);
//date validation

let date = new Date();
let year = date.getFullYear();
let month = date.getMonth() + 1;
let day = date.getDate();
year = 2015;
console.log(year, month, day)

if (month < 10) {
    month = `0` + month;
}
if (day < 12) {
    day = `0` + day;
}
let maxDate = `${year}-${month}-${day}`;
document.getElementById("dobId").setAttribute("max", maxDate);


let countryselectbox = document.createElement("SELECT");
countryselectbox.setAttribute("id", "countryName");
document.getElementById("countryContainerId").appendChild(countryselectbox);

let countrylist = document.createElement("option");
countrylist.setAttribute("value", "select");
countrylist.innerHTML = "select";
document.getElementById("countryName").appendChild(countrylist);
for (let i = 0; i < locationData.length; i++) {
    let countrylist = document.createElement("option");
    countrylist.setAttribute("value", locationData[i].country);

    countrylist.innerHTML = locationData[i].country;
    document.getElementById("countryName").appendChild(countrylist);

}
// document.getElementById("stateId").disabled = true;
// document.getElementById("cityId").disabled = true;

let selectbox = document.createElement("SELECT");
selectbox.setAttribute("id", "stateName");
document.getElementById("stateContainerId").appendChild(selectbox);

let statelist = document.createElement("option");
statelist.setAttribute("value", "select");
statelist.innerHTML = "select";
document.getElementById("stateName").appendChild(statelist);

for (let i = 0; i < states.length; i++) {
    let statelist = document.createElement("option");
    statelist.setAttribute("value", states[i]);
    statelist.innerHTML = states[i]
    document.getElementById("stateName").appendChild(statelist);
}

const addressContainer = document.createElement('div');
addressContainer.className = "addressContainer"
addressContainer.innerHTML = ` <div class="communicationAddress">
    Communication Address :  <span class="starStyle">*</span><br>
    <textarea name="" id="communicationAddr" style="resize:none" onkeypress="errorMessageRemove(event)"></textarea><br><span class="error" id="errorcommunication"></span><br></div>
    <div class="permanentAddress">
    Permanent Address :  <span class="starStyle">*</span><br>
    <textarea name="" id="permanentAddr" style="resize:none" onkeypress="errorMessageRemove(event)"></textarea><br><span class="error" id="errorpermanent"></span><br></div>`;
containerId.appendChild(addressContainer);


const sameAsAddress = document.createElement('div');
sameAsAddress.className = "sameAsAddress"
sameAsAddress.innerHTML = `<input type = "checkbox"  id = "sameAddrId" onchange="sameAsFunction()"> Permanent address same as communication address`
containerId.appendChild(sameAsAddress);

const pincodeContainer = document.createElement('div');
pincodeContainer.className = "pincodeContainer"
pincodeContainer.innerHTML = `<br>Pincode : <span class="starStyle">*</span><br> <input type="text" class="pincodeClass" id="pincodeId"  onkeypress="errorMessageRemove(event)" maxlength="6" onkeypress="return numericOnly(event);"> <br><span class="error" id="errorpincode"></span><br>`
containerId.appendChild(pincodeContainer);

const submitContainer = document.createElement('div');
submitContainer.className = "submitContainer"
submitContainer.innerHTML = `<button type="submit" onclick="register(event)" class="registerClass" id="registerId">Register</button>
<button type="submit" onclick="cancel()" class="cancelClass">Cancel</button>`
containerId.appendChild(submitContainer);

//auto fill permanenet address
function sameAsFunction() {
    let data = document.getElementById('sameAddrId').checked;
    document.getElementById("permanentAddr").disabled = true;


    if (data == true) {

        document.getElementById("permanentAddr").value = document.getElementById("communicationAddr").value;

    } else {
        document.getElementById("permanentAddr").value = "";
        document.getElementById("permanentAddr").disabled = false;


    }
}

communicationAddr.addEventListener('input', sameAsFunction);

let imageSrc = document.getElementById('uploadImageId').src;
// console.log(imageSrc + "image");
selectData();

console.log(arr)

function register(event) {
    event.preventDefault();
    console.log(formDataDetail);
    document.getElementById('errorOraganizationName').innerHTML = "";
    document.getElementById('errorfirstName').innerHTML = "";
    document.getElementById('errorlastName').innerHTML = "";
    document.getElementById('errorgender').innerHTML = "";
    document.getElementById('errordob').innerHTML = "";
    document.getElementById('errormobile').innerHTML = "";
    document.getElementById('errormail').innerHTML = "";
    document.getElementById('errorcountry').innerHTML = "";
    document.getElementById('errorstate').innerHTML = "";
    document.getElementById('errorcity').innerHTML = "";
    document.getElementById('errorcommunication').innerHTML = "";
    document.getElementById('errorpermanent').innerHTML = "";
    document.getElementById('errorpincode').innerHTML = "";

    const select = document.getElementById('myselect');
    const option = select.options[select.selectedIndex];
    const organizationValue = option.text;


    let firstName = document.getElementById('firstNameinputId').value;
    let lastName = document.getElementById('lastNameinputId').value;
    let genderCheck = document.getElementsByName('gender');

    let userdob = document.getElementById('dobId').value;
    let usermobileNumberId = document.getElementById('mobileNumberId').value;
    let usermailId = document.getElementById('mailId').value;
    const countryName = document.getElementById('countryName');
    const stateName = document.getElementById('stateName');
    let usercityId = document.getElementById('cityId').value;
    let usercommunicationAddress = document.getElementById('communicationAddr').value;
    let userpermanentAddress = document.getElementById('permanentAddr').value;
    let userpincode = document.getElementById('pincodeId').value;
    for (i = 0; i < genderCheck.length; i++) {
        if (genderCheck[i].checked)

            genderSelect = genderCheck[i].value
    }
    const countryNameoption = countryName.options[countryName.selectedIndex];
    const countryNameValue = countryNameoption.text;


    const stateNameoption = stateName.options[stateName.selectedIndex];
    const stateNameValue = stateNameoption.text;


    if (organizationValue === 'Select organization' || !firstName || !lastName || !genderSelect || !userdob ||
        !usermobileNumberId || !usermailId || countryNameValue === 'select' || stateNameValue === 'select' ||
        !usercityId || !usercommunicationAddress || !userpermanentAddress || !userpincode) {
        if (organizationValue === 'Select organization') {
            document.getElementById('errorOraganizationName').innerHTML = "Please select organization field";
        }
        if (!firstName) {
            document.getElementById('errorfirstName').innerHTML = "Please enter first name";
        }
        if (!lastName) {
            document.getElementById('errorlastName').innerHTML = "Please enter last Name";
        }
        if (!genderSelect) {
            document.getElementById('errorgender').innerHTML = "Please gender option select";
        }
        if (!userdob) {
            document.getElementById('errordob').innerHTML = "Please enter date of birth";
        }
        if (!usermobileNumberId) {
            document.getElementById('errormobile').innerHTML = "Please enter mobile number";
        }
        if (!usermailId) {
            document.getElementById('errormail').innerHTML = "Please enter email Id";
        }
        if (countryNameValue === 'select') {
            document.getElementById('errorcountry').innerHTML = "Please enter country name";
        }
        if (stateNameValue === 'select') {
            document.getElementById('errorstate').innerHTML = "Please enter state name";
        }
        if (!usercityId) {
            document.getElementById('errorcity').innerHTML = "Please enter city name";
        }
        if (!usercommunicationAddress) {
            document.getElementById('errorcommunication').innerHTML = "Please enter communication address";
        }
        if (!userpermanentAddress) {

            document.getElementById('errorpermanent').innerHTML = "Please enter permanent address";
        }
        if (!userpincode) {
            document.getElementById('errorpincode').innerHTML = "Please enter pincode";
        }
        if (usermailId) {
            let emailString = document.getElementById('mailId').value;
            let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (mailformat.test(emailString)) {
                document.getElementById('errormail').innerHTML = "";
            } else {
                document.getElementById('errormail').innerHTML = "please enter valid email address!";
            }
        }
    } else {

        let imageSrc = document.getElementById('uploadImageId').src;
        console.log(imageSrc);
        let fullname = firstName.trim() + " " + lastName.trim();
        formDataDetail = {
            image: profileImageUrl,
            organization: organizationValue,
            name: fullname,
            gender: genderSelect,
            dob: userdob,
            mobile: usermobileNumberId,
            mailId: usermailId,
            countryName: countryNameValue,
            stateName: stateNameValue,
            city: usercityId.trim(),
            communicationAddress: usercommunicationAddress.trim(),
            permanentAddress: userpermanentAddress.trim(),
            pincode: userpincode.trim()
        };

        arr.push(formDataDetail);
        localStorage.setItem("crud", JSON.stringify(arr));
        console.log(arr);

        selectData();

        // console.log(deleteData(event));
        // document.getElementById('registerId').innerHTML = "Register";
        document.getElementById("resetForm").reset();

    }

}



function selectData() {

    let getDataValue = getCrudData()

    if (getDataValue != null) {
        let html = '';
        let sno = 1;

        for (let i = 0; i < getDataValue.length; i++) {
            html = html + `<tr><td>${sno}</td><td>${getDataValue[i].name}</td><td>${getDataValue[i].gender}</td><td>${getDataValue[i].dob}</td><td>${getDataValue[i].mobile}</td><td>${getDataValue[i].mailId}</td><td><a href="javascript:void(0)" onclick="editData(${i})">Edit</a>/<a href="javascript:void(0)" onclick="deleteData(${i})">Delete</a></td></tr>`;
            sno++;
        }
        document.getElementById('root').innerHTML = html;
    }
}

function getCrudData() {
    let arr = JSON.parse(localStorage.getItem('crud'));
    return arr;
}

function editData(rid) {

    console.log(profileImageUrl)
    id = rid;
    let arr = getCrudData();
    document.getElementById('profileImage').src = arr[rid].image;
    document.getElementById('myselect').value = arr[rid].organization;
    document.getElementById('countryName').value = arr[rid].countryName;
    document.getElementById('stateName').value = arr[rid].stateName;
    console.log(arr[rid].organization)

    let name = arr[rid].name.split(" ");

    document.getElementById('firstNameinputId').value = name[0];
    document.getElementById('lastNameinputId').value = name[1];
    console.log(arr[rid].gender)
    if (arr[rid].gender == 'Male')
        document.getElementById('male').checked = arr[rid].gender;
    else {
        document.getElementById('female').checked = arr[rid].gender;
    }

    document.getElementById('dobId').value = arr[rid].dob;

    document.getElementById('mobileNumberId').value = arr[rid].mobile;


    document.getElementById('mailId').value = arr[rid].mailId;

    document.getElementById('cityId').value = arr[rid].city;


    document.getElementById('communicationAddr').value = arr[rid].communicationAddress;

    document.getElementById('permanentAddr').value = arr[rid].permanentAddress;
    if (arr[rid].communicationAddress === arr[rid].permanentAddress) {
        document.getElementById('sameAddrId').checked = true;
        document.getElementById('permanentAddr').disabled = true;
    }

    document.getElementById('pincodeId').value = arr[rid].pincode;
    document.getElementById('registerId').innerHTML = "Update";
    document.getElementById('registerId').onclick = () => {

        register(rid);

        document.getElementById("resetForm").reset();
    }



}

function deleteData(rid) {
    let arr = getCrudData();
    arr.splice(rid, 1);
    setCrudData(arr);
    selectData();
}

function setCrudData(arr) {
    localStorage.setItem('crud', JSON.stringify(arr));
}

// let arr = [];

// let locationData = [{ country: "India" }];
// let formDataDetail = [];
// let profileImageUrl;
// let genderSelect;
// let states = ["Tamil Nadu", "Andhra pradesh", "karnataka", "kerala"]
// const genderInput = `Male <input type="radio" id="male" name="gender" value="Male" required> ` + `Female <input type="radio" id="female" name="gender" value="Female"> `;
// const profileContainer = ` <div class="photoContainer">
// <div class="photoUploadContainer">
// <div class="photo">
// <img src="" alt="" id="profileImage"></div>
// <div class="photoUploadButton" id = 'photoUploadButtonId'>
// </div>
// <span class="error" id="errorImage"></span>
// </div>

// </div>
// <div class="organizationContainer">
// <div class="organizationName" id='organizationNameId'>Organization Name : *<br>
// </div><span class="error"  id="errorOraganizationName"></span>
// <div class="firstName" id="firstNameId"></div>
// </div>
// <div class="nameContainer">
// <div class="lastName" id="lastNameId"></div>
// <div class="gender" id="genderId"></div></div>`;
// const organizationData = ['VirdhiTech', 'Google', 'Facebook', 'Whatsapp']
// const containerId = document.getElementById('containerId');
// const profileContainerDiv = document.createElement('div');
// profileContainerDiv.className = "profileContainer"
// profileContainerDiv.innerHTML = profileContainer;
// containerId.appendChild(profileContainerDiv);
// photoUploadButtonId.innerHTML = `<input type="file" class ="uploadButton" id="uploadImageId" accept="image/png, image/jpeg">`


// let profileimage = document.getElementById('profileImage');
// let uploadButton = document.getElementById('uploadImageId');
// uploadButton.addEventListener('change', function() {

//     profileimage.style.display = 'block';
//     profileimage.src = URL.createObjectURL(this.files[0])
//     profileImageUrl = profileimage.src;
//     formDataDetail.push({ image: profileImageUrl });
//     console.log(formDataDetail);


//     // console.log({ image: profileImageUrl })

// })

// const organizationNameId = document.getElementById('organizationNameId');
// let organizationNameIdSelect = document.createElement("SELECT");
// organizationNameIdSelect.setAttribute("id", "myselect");
// organizationNameId.appendChild(organizationNameIdSelect);

// let organizationOption = document.createElement("option");
// organizationOption.setAttribute("value", "name");
// organizationOption.innerHTML = "Select organization";
// document.getElementById("myselect").appendChild(organizationOption);
// for (let i = 0; i < organizationData.length; i++) {
//     let organizationOption = document.createElement("option");
//     organizationOption.setAttribute("value", "name");

//     organizationOption.innerHTML = organizationData[i];
//     document.getElementById("myselect").appendChild(organizationOption);
// }
// firstNameId.innerHTML = "First Name :  *<br> " + `<input type="text" class="firstNameinput" id="firstNameinputId" onkeydown="return alphaOnly(event)" <br> <span class="error" id="errorfirstName"></span>`;
// lastNameId.innerHTML = "Last Name :  *<br> " + `<input type="text" class="lastNameinput" id="lastNameinputId" onkeydown="return alphaOnly(event)" required><br> <span class="error" id="errorlastName"></span>`;
// genderId.innerHTML = "Gender :  * <br>" + genderInput + `<br><span class="error" id="errorgender"></span>`;
// // const genderId = document.getElementById('genderId')



// function alphaOnly(event) {
//     let key = event.keyCode;
//     return ((key >= 65 && key <= 90) || key == 8 || key == 9);
// };

// function numericOnly(event) {
//     let key = event.keyCode;
//     return (key >= 48 && key <= 57);

// }
// const ContactDetailContainerDiv = document.createElement('div');
// ContactDetailContainerDiv.className = "ContactDetailContainer";
// ContactDetailContainerDiv.innerHTML =
//     `<div class="dobContainer" id="dobContainerId">Date of Birth :  *<br>
// <input type="date" class="dobClass" id="dobId"required> <br><span class="error" id="errordob" min="2000-01-02"></span>
// </div>
// <div class="mobileContainer" id="mobileContainerId">Mobile Number :  *<br>
// <input type="text" class="mobileNumber" id=mobileNumberId name="mobileNumber" maxlength="10" onkeypress="return numericOnly(event)" required ><br><span class="error" id="errormobile"></span>
// </div>
// <div class="mailIdContainer" id="mailIdContainerId">Email ID:  *<br>
// <input type="email" class="mailIdClass" id="mailId" required><br><span class="error" id="errormail"></span><br>
// </div>`
// containerId.appendChild(ContactDetailContainerDiv);

// const locationContainer = document.createElement('div');
// locationContainer.className = "locationContainer"
// locationContainer.innerHTML = `<div><div class="countryContainer " id="countryContainerId">
// Country:   * <br> 

// </div>
// <span class="error" id="errorcountry"><br>  
// </div>

// <div><div class="stateContainer" id="stateContainerId" >
//   State:  * <br>
// </div>
// <span class="error" id="errorstate"><br>
// </div>
// <div class="cityContainer" id="cityIdContainerId">City : *<br>
//     <input type="text" class="cityClass" id="cityId" onkeydown="return alphaOnly(event)" required> <br><span class="error" id="errorcity"></span>
// </div>`;
// containerId.appendChild(locationContainer);


// let countryselectbox = document.createElement("SELECT");
// countryselectbox.setAttribute("id", "countryName");
// document.getElementById("countryContainerId").appendChild(countryselectbox);

// let countrylist = document.createElement("option");
// countrylist.setAttribute("value", "name");
// countrylist.innerHTML = "select";
// document.getElementById("countryName").appendChild(countrylist);
// for (let i = 0; i < locationData.length; i++) {
//     let countrylist = document.createElement("option");
//     countrylist.setAttribute("value", "name");

//     countrylist.innerHTML = locationData[i].country;
//     document.getElementById("countryName").appendChild(countrylist);

// }
// // document.getElementById("stateId").disabled = true;
// // document.getElementById("cityId").disabled = true;

// let selectbox = document.createElement("SELECT");
// selectbox.setAttribute("id", "stateName");
// document.getElementById("stateContainerId").appendChild(selectbox);

// let statelist = document.createElement("option");
// statelist.setAttribute("value", "name");
// statelist.innerHTML = "select";
// document.getElementById("stateName").appendChild(statelist);
// for (let i = 0; i < 4; i++) {
//     let statelist = document.createElement("option");
//     statelist.setAttribute("value", "name");
//     statelist.innerHTML = states[i]
//     document.getElementById("stateName").appendChild(statelist);
// }

// const addressContainer = document.createElement('div');
// addressContainer.className = "addressContainer"
// addressContainer.innerHTML = ` <div class="communicationAddress">
//     Communication Address :  *<br>
//     <textarea name="" id="communicationAddr" style="resize:none"></textarea><br><span class="error" id="errorcommunication"></span><br></div>
//     <div class="permanentAddress">
//     Permanent Address :  *<br>
//     <textarea name="" id="permanentAddr" style="resize:none"></textarea><br><span class="error" id="errorpermanent"></span><br></div>`;
// containerId.appendChild(addressContainer);


// const sameAsAddress = document.createElement('div');
// sameAsAddress.className = "sameAsAddress"
// sameAsAddress.innerHTML = `<input type = "checkbox"  id = "sameAddrId" onchange="sameAsFunction()"> Permanent address same as communication address`
// containerId.appendChild(sameAsAddress);

// const pincodeContainer = document.createElement('div');
// pincodeContainer.className = "pincodeContainer"
// pincodeContainer.innerHTML = `<br>Pincode : *<br> <input type="text" class="pincodeClass" id="pincodeId" maxlength="6" onkeypress="return numericOnly(event);" > <br><span class="error" id="errorpincode"></span><br>`
// containerId.appendChild(pincodeContainer);

// const submitContainer = document.createElement('div');
// submitContainer.className = "submitContainer"
// submitContainer.innerHTML = `<button type="submit" onclick="register()" class="registerClass" id="registerId">Register</button>
// <button type="submit" onclick="cancel()" class="cancelClass">Cancel</button>`
// containerId.appendChild(submitContainer);



// //auto fill permanenet address
// function sameAsFunction() {
//     let data = document.getElementById('sameAddrId').checked;
//     document.getElementById("permanentAddr").disabled = true;


//     if (data == true) {

//         document.getElementById("permanentAddr").value = document.getElementById("communicationAddr").value;

//     } else {
//         document.getElementById("permanentAddr").value = "";
//         document.getElementById("permanentAddr").disabled = false;


//     }
// }

// communicationAddr.addEventListener('input', sameAsFunction);

// let imageSrc = document.getElementById('uploadImageId').src;
// // console.log(imageSrc + "image");
// selectData();


// function register() {

//     document.getElementById('errorOraganizationName').innerHTML = "";
//     document.getElementById('errorfirstName').innerHTML = "";
//     document.getElementById('errorlastName').innerHTML = "";
//     document.getElementById('errorgender').innerHTML = "";
//     document.getElementById('errordob').innerHTML = "";
//     document.getElementById('errormobile').innerHTML = "";
//     document.getElementById('errormail').innerHTML = "";
//     document.getElementById('errorcountry').innerHTML = "";
//     document.getElementById('errorstate').innerHTML = "";
//     document.getElementById('errorcity').innerHTML = "";
//     document.getElementById('errorcommunication').innerHTML = "";
//     document.getElementById('errorpermanent').innerHTML = "";
//     document.getElementById('errorpincode').innerHTML = "";

//     const select = document.getElementById('myselect');
//     const option = select.options[select.selectedIndex];
//     const organizationValue = option.text;


//     let firstName = document.getElementById('firstNameinputId').value;
//     let lastName = document.getElementById('lastNameinputId').value;
//     let genderCheck = document.getElementsByName('gender');

//     let userdob = document.getElementById('dobId').value;
//     let usermobileNumberId = document.getElementById('mobileNumberId').value;
//     let usermailId = document.getElementById('mailId').value;
//     const countryName = document.getElementById('countryName');
//     const stateName = document.getElementById('stateName');
//     let usercityId = document.getElementById('cityId').value;
//     let usercommunicationAddress = document.getElementById('communicationAddr').value;
//     let userpermanentAddress = document.getElementById('permanentAddr').value;
//     let userpincode = document.getElementById('pincodeId').value;
//     for (i = 0; i < genderCheck.length; i++) {
//         if (genderCheck[i].checked)

//             genderSelect = genderCheck[i].value
//     }
//     const countryNameoption = countryName.options[countryName.selectedIndex];
//     const countryNameValue = countryNameoption.text;


//     const stateNameoption = stateName.options[stateName.selectedIndex];
//     const stateNameValue = stateNameoption.text;


//     if (organizationValue === 'Select organization' || !firstName || !lastName || !genderSelect || !userdob ||
//         !usermobileNumberId || !usermailId || countryNameValue === 'select' || stateNameValue === 'select' ||
//         !usercityId || !usercommunicationAddress || !userpermanentAddress || !userpincode) {
//         if (organizationValue === 'Select organization') {
//             document.getElementById('errorOraganizationName').innerHTML = "Please select organization field";
//         }
//         if (!firstName) {
//             document.getElementById('errorfirstName').innerHTML = "Please enter first name";
//         }
//         if (!lastName) {
//             document.getElementById('errorlastName').innerHTML = "Please enter last Name";
//         }
//         if (!genderSelect) {
//             document.getElementById('errorgender').innerHTML = "Please gender option select";
//         }
//         if (!userdob) {
//             document.getElementById('errordob').innerHTML = "Please enter date of birth";
//         }
//         if (!usermobileNumberId) {
//             document.getElementById('errormobile').innerHTML = "Please enter mobile number";
//         }
//         if (!usermailId) {
//             document.getElementById('errormail').innerHTML = "Please enter email Id";
//         }
//         if (countryNameValue === 'select') {
//             document.getElementById('errorcountry').innerHTML = "Please enter country name";
//         }
//         if (stateNameValue === 'select') {
//             document.getElementById('errorstate').innerHTML = "Please enter state name";
//         }
//         if (!usercityId) {
//             document.getElementById('errorcity').innerHTML = "Please enter city name";
//         }
//         if (!usercommunicationAddress) {
//             document.getElementById('errorcommunication').innerHTML = "Please enter communication address";
//         }
//         if (!userpermanentAddress) {

//             document.getElementById('errorpermanent').innerHTML = "Please enter permanent address";
//         }
//         if (!userpincode) {
//             document.getElementById('errorpincode').innerHTML = "Please enter pincode";
//         }

//     } else {
//         let imageSrc = document.getElementById('uploadImageId').src;
//         console.log(imageSrc);
//         let fullname = firstName + " " + lastName;
//         formDataDetail = {
//             organization: organizationValue,
//             name: fullname,
//             gender: genderSelect,
//             dob: userdob,
//             mobile: usermobileNumberId,
//             mailId: usermailId,
//             countryName: countryNameValue,
//             stateName: stateNameValue,
//             city: usercityId,
//             communicationAddress: usercommunicationAddress,
//             permanentAddress: userpermanentAddress,
//             pincode: userpincode
//         };
//         arr.push(formDataDetail);
//         localStorage.setItem("crud", JSON.stringify(arr));


//         selectData();
//         document.getElementById('registerId').innerHTML = "Register";
//         // document.getElementById("resetForm").reset();

//     }
// }

// function selectData() {

//     let getDataValue = getCrudData()

//     if (getDataValue != null) {
//         let html = '';
//         let sno = 1;

//         for (let i = 0; i < getDataValue.length; i++) {
//             html = html + `<tr><td>${sno}</td><td>${getDataValue[i].name}</td><td>${getDataValue[i].gender}</td><td>${getDataValue[i].dob}</td><td>${getDataValue[i].mobile}</td><td>${getDataValue[i].mailId}</td><td><a href="javascript:void(0)" onclick="editData(${i})">Edit</a>/<a href="javascript:void(0)" onclick="deleteData(${i})">Delete</a></td></tr>`;
//             sno++;
//         }
//         document.getElementById('root').innerHTML = html;
//     }
// }

// function getCrudData() {
//     let arr = JSON.parse(localStorage.getItem('crud'));
//     return arr;
// }

// function editData(rid) {
// id = rid;
// let arr = getCrudData();

// document.getElementById('myselect').value = arr[rid].organization;

// let name = arr[rid].name.split(" ");

// document.getElementById('firstNameinputId').value = name[0];
// document.getElementById('lastNameinputId').value = name[1];
// console.log(arr[rid].gender)
// if (arr[rid].gender == 'Male')
//     document.getElementById('male').checked = arr[rid].gender;
// else {
//     document.getElementById('female').checked = arr[rid].gender;
// }

// document.getElementById('dobId').value = arr[rid].dob;

// document.getElementById('mobileNumberId').value = arr[rid].mobile;


// document.getElementById('mailId').value = arr[rid].mailId;

// document.getElementById('cityId').value = arr[rid].city;


// document.getElementById('communicationAddr').value = arr[rid].communicationAddress;

// document.getElementById('permanentAddr').value = arr[rid].permanentAddress;


// document.getElementById('pincodeId').value = arr[rid].pincode;
// document.getElementById('registerId').innerHTML = "Update";
// document.getElementById('registerId').onclick(function() {
//         register();

//     })
//     // deleteData(rid);
// console.log(document.getElementById('myselect'))
// console.log(document.getElementById('stateName'))
// }

// function deleteData(rid) {
//     let arr = getCrudData();
//     arr.splice(rid, 1);
//     setCrudData(arr);
//     selectData();
// }

// function setCrudData(arr) {
//     localStorage.setItem('crud', JSON.stringify(arr));
// }

function cancel() {
    document.getElementById('profileImage').style.display = "none"
    profileimage.src = '';
    document.getElementById('registerId').innerHTML = "Register";
    document.getElementById("resetForm").reset();

}