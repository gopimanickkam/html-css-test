let coordinatesArray = [];

function addCoordinates() {

    let coordinatesValue = parseInt(document.getElementById('coordinatesValue').value);
    coordinatesArray.push(coordinatesValue);
    document.getElementById('coordinates').innerHTML = JSON.stringify(coordinatesArray);
}


function calculatePosition() {
    let
        direction = [0, 0],
        count = 0;
    coordinatesArray.forEach((item, index) => {
        if (index % 2 === 0) {
            count++;

            if (count % 2 === 0) //direction of x-axis
                return direction[1] -= item;
            direction[1] += item;

        } else {

            if (count % 2 === 0) //direction of y-axis
                return direction[0] -= item;
            direction[0] += item;
        }

    });
    document.getElementById("currentPosition").innerHTML = "The current position of robot is " + JSON.stringify(direction);
}