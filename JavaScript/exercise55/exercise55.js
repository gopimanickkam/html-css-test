function primeNumber() {

    for (let i = 2; i <= 1000; i++) {
        let isprime = true;
        for (let j = 2; j <= i / 2; j++) {
            if (i % j == 0) {
                isprime = false;
                break;
            }

        }
        if (isprime) {
            document.write(i + "<br>");
        }
    }
}