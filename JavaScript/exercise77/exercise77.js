function calculate() {
    let radius = document.getElementById('radius').value;
    let height = document.getElementById('height').value;
    document.getElementById('volume').innerHTML = "The volume of a cylinder is : " + (Math.PI * radius * radius * height).toFixed(4);
    document.getElementById('tsa').innerHTML = "Total Surface Area of a cyclinder is : " + (2 * Math.PI * radius * (height + radius)).toFixed(4);
    document.getElementById('csa').innerHTML = "Curved Surface Area of a Cyclinder is : " + (2 * Math.PI * radius * height).toFixed(4);
}