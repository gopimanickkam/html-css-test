let range = function(startNum, endNum) {
    if (endNum - startNum === 2) {
        return [startNum + 1];
    }
    let list = range(startNum, endNum - 1);
    list.push(endNum - 1);
    return list;

};

console.log(range(2, 9));