function humanize() {
    let num1 = document.getElementById('num1').value;
    if ((num1 % 100 >= 11 && num1 % 100 <= 13) || !(num1 % 10) || (num1 % 10) > 3)
        return document.getElementById('result').innerHTML = num1 + "th";

    switch (num1 % 10) {
        case 1:
            return document.getElementById('result').innerHTML = num1 + "st";
        case 2:
            return document.getElementById('result').innerHTML = num1 + "nd";
        case 3:
            return document.getElementById('result').innerHTML = num1 + "rd";
    }

    // return document.getElementById('result').innerHTML = num1 + "th";
}