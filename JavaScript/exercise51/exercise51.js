function isPowerOfTwo() {
    let n = document.getElementById('num1').value;
    if (n == 0)
        return document.getElementById('result').innerHTML = "The given number is not power of 2";
    while (n != 1) {
        if (n % 2 != 0)
            return document.getElementById('result').innerHTML = "The given number is not power of 2";
        n = n / 2;
    }
    return document.getElementById('result').innerHTML = "The given number is power of 2";
}