function checkPassword() {
    let password = document.getElementById("password").value;
    let passwordRegex = new RegExp(/^(?!.*([A-Za-z0-9])\1{2})(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z0-9]{6,24}/);
    if (passwordRegex.test(password)) {

        return document.getElementById('result').innerHTML = "Valid password";
    }

    document.getElementById('result').innerHTML = "Invalid Password";


}