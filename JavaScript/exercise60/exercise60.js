function primeNumber() {
    let n = document.getElementById('num1').value;
    for (let i = 2; i <= n; i++) {
        let isprime = 1;
        for (let j = 2; j <= i / 2; j++) {
            if (i % j == 0) {
                isprime = 0;
                break;
            }

        }
        if (isprime == 1) {
            document.write(i + "<br>");
        }
    }
}