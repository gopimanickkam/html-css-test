function searchWord() {
    let textName = document.getElementById('textName').value;
    let wordName = document.getElementById('wordName').value;
    let x = 0,
        y = 0;

    for (let i = 0; i < textName.length; i++) {
        if (textName[i] == wordName[0]) {
            for (let j = i; j < i + wordName.length; j++) {
                if (textName[j] == wordName[j - i]) {
                    y++;
                }
                if (y == wordName.length) {
                    x++;
                }
            }
            y = 0;
        }
    }
    document.getElementById('result').innerHTML = wordName + "' was found " + x + " times.";
}