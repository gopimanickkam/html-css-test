function uppercaseString() {
    let str = document.getElementById('str').value;
    let regexp = /^[A-Z]/;
    if (regexp.test(str)) {
        return document.getElementById('result').innerHTML = "String's first character is uppercase";
    }
    document.getElementById('result').innerHTML = "String's first character is not uppercase";

}